# Nix config

## Getting started

### Build / Switch

Build all nodes.


```bash
$ build
```

Switch to the current configuraion.

```bash
$ switch
```

### Update

Run `nix flake update` and `nvfetcher`.

```bash
$ update
```
