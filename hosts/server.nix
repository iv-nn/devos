{ self, config, ... }:
{
  imports =
    self.nixosSuites.server
    ++ (with self.nixosProfiles; [
      server.media.default
      server.torrent.default
    ])
    ++ (with self.users; [
      root.default
      ivann.default
    ]);

  home-manager.users.ivann =
    { ... }:
    {
      imports = self.homeSuites.base;
    };

  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    supportedFilesystems = [ "zfs" ];
    kernelParams = [ "ip=dhcp" ];
    initrd = {
      supportedFilesystems = [ "zfs" ];
      kernelModules = [ "zfs" ];
      luks.devices = {
        root = {
          device = "/dev/disk/by-uuid/0074752c-92cb-41be-b8e3-c695304bdf17";
          preLVM = true;
          allowDiscards = true;
        };
      };
      availableKernelModules = [ "r8169" ];
      network = {
        enable = true;
        # FIXME: find a way to start `systemctl default` on login
        ssh = {
          enable = true;
          port = 22222;
          authorizedKeys = config.users.users.ivann.openssh.authorizedKeys.keys;
          hostKeys = [ "/etc/ssh/initrd_ssh_host_ed25519_key" ];
        };
      };
      systemd = {
        enable = true;
        # wait for disk being decrypted before import zfs root
        services."zfs-import-root" = {
          after = [ "cryptsetup.target" ];
          wants = [ "cryptsetup.target" ];
        };
      };
    };
    tmp.useTmpfs = true;
  };

  hardware = {
    enableRedistributableFirmware = true;
    graphics.enable = true;
  };

  stylix = {
    enable = false;
    image = ../white.png;
  };

  networking = {
    hostId = "334bbd88";
    hostName = "server";
  };

  fileSystems = {
    root = {
      mountPoint = "/";
      device = "rpool/root";
      fsType = "zfs";
    };

    boot = {
      mountPoint = "/boot";
      device = "/dev/disk/by-uuid/FFE4-F4C1";
      fsType = "vfat";
      options = [
        "umask=0077"
        "defaults"
      ];
    };

    media = {
      mountPoint = "/var/media";
      device = "storage/media";
      fsType = "zfs";
    };
  };

  swapDevices = [ { device = "/dev/disk/by-uuid/0363df62-4ab8-459c-b246-eddcd9ad3f76"; } ];
}
