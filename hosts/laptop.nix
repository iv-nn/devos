{ self, pkgs, ... }:
{
  imports =
    self.nixosSuites.desktop
    ++ (with self.inputs.nixos-hardware.nixosModules; [
      common-pc-ssd
      common-cpu-intel
    ])
    ++ (with self.nixosProfiles; [
      laptop.default
      wireless.default
      bluetooth.default
    ])
    ++ (with self.users; [
      root.default
      ivann.default
    ]);

  home-manager.users.ivann =
    { ... }:
    {
      imports =
        (with self.homeSuites; base ++ dev ++ desktop)
        ++ (with self.homeProfiles; [
          email.protonmail.default
          perso.default
        ]);
    };

  # Use the systemd-boot EFI boot loader.
  boot = {
    loader = {
      systemd-boot.enable = true;
      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot/efi";
      };
    };

    initrd = {
      availableKernelModules = [
        "xhci_pci"
        "ahci"
        "nvme"
        "usb_storage"
        "sd_mod"
        "rtsx_usb_sdmmc"
      ];
      kernelModules = [ "dm-snapshot" ];
      luks.devices = {
        root = {
          device = "/dev/disk/by-uuid/4b94dab1-c479-4f5b-9d78-1b4507f0ea2c";
          preLVM = true;
          allowDiscards = true;
        };
      };
    };

    blacklistedKernelModules = [ "nouveau" ];
    kernelModules = [ "kvm-intel" ];
  };

  networking = {
    hostName = "laptop";
  };

  hardware.enableRedistributableFirmware = true;

  services = {
    xserver = {
      videoDrivers = [ "intel" ];
    };

    actkbd = {
      enable = true;
      bindings = [
        {
          keys = [ 232 ];
          events = [ "key" ];
          command = "${pkgs.light}/bin/light -U 10";
        }
        {
          keys = [ 233 ];
          events = [ "key" ];
          command = "${pkgs.light}/bin/light -A 10";
        }
      ];
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/499fd80c-39d6-4f82-a49a-c4fefae26733";
      fsType = "ext4";
      options = [
        "noatime"
        "nodiratime"
      ];
    };

    "/home" = {
      device = "/dev/disk/by-uuid/4ead1167-404e-4d1b-a7ca-04d20331f764";
      fsType = "ext4";
      options = [
        "noatime"
        "nodiratime"
      ];
    };

    "/boot/efi" = {
      device = "/dev/disk/by-uuid/8A89-62F0";
      fsType = "vfat";
    };
  };

  swapDevices = [ { device = "/dev/disk/by-uuid/a2c7728e-c7fe-4a46-aaac-8bab57f25789"; } ];

  nix.settings.max-jobs = 12;
}
