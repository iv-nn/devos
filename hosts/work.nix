{ self, ... }:
{
  imports =
    self.nixosSuites.desktop
    ++ (with self.inputs.nixos-hardware.nixosModules; [
      common-pc-laptop
      common-pc-ssd
      common-cpu-intel
    ])
    ++ (with self.nixosProfiles; [
      bluetooth.default
      laptop.default
      wireless.default
    ])
    ++ (with self.users; [
      root.default
      ivann.default
    ]);

  home-manager.users.ivann =
    { ... }:
    {
      imports =
        (with self.homeSuites; base ++ dev ++ desktop) ++ (with self.homeProfiles; [ work.default ]);

      wayland.windowManager.hyprland.settings = {
        monitor = [
          "eDP-1, preferred, auto-right, 1"
          ", preferred, auto-left, 1"
        ];
      };

      services.gammastep.enable = false;
    };

  # Use the systemd-boot EFI boot loader.
  boot = {
    loader = {
      systemd-boot.enable = true;
      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot/efi";
      };
    };

    kernelParams = [
      "zswap.enabled=1"
      "zswap.zpool=zsmalloc"
    ];

    initrd = {
      availableKernelModules = [
        "xhci_pci"
        "thunderbolt"
        "vmd"
        "nvme"
        "usb_storage"
        "sd_mod"
        "rtsx_usb_sdmmc"
      ];
      kernelModules = [ "dm-snapshot" ];
      luks = {
        devices = {
          root = {
            device = "/dev/disk/by-uuid/40b4b6df-7257-45ea-b5c1-f5ef95df7f0b";
            preLVM = true;
            allowDiscards = true;
          };
        };
      };
    };
  };

  networking = {
    hostName = "work";
  };

  hardware.enableRedistributableFirmware = true;

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/6bdd3926-afdd-4c42-9b37-01674098c66c";
      fsType = "ext4";
      options = [
        "noatime"
        "nodiratime"
      ];
    };

    "/boot/efi" = {
      device = "/dev/disk/by-uuid/DB36-A6C2";
      fsType = "vfat";
    };
  };

  swapDevices = [ { device = "/dev/disk/by-uuid/5abb24e2-0bff-44fa-8397-178f0e5e1a18"; } ];

  nix.settings.max-jobs = 4;
}
