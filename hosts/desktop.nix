{ self, pkgs, ... }:
{
  imports =
    self.nixosSuites.desktop
    ++ (with self.inputs.nixos-hardware.nixosModules; [
      common-pc-ssd
      common-cpu-amd
      common-gpu-amd
    ])
    ++ (with self.nixosProfiles; [
      bluetooth.default
      game.default
    ])
    ++ (with self.users; [
      root.default
      ivann.default
    ]);

  home-manager.users.ivann =
    { ... }:
    {
      imports =
        (with self.homeSuites; base ++ dev ++ desktop)
        ++ (with self.homeProfiles; [
          email.protonmail.default
          perso.default
        ])
        ++ [ "${self}/secrets/users/ivann/private.nix" ];

      services.pass-secret-service.enable = true;

      wayland.windowManager.hyprland.settings = {
        monitor = [
          "DP-1, highrr, auto-right, 1"
          "HDMI-A-1, highres, auto-left, 1"
        ];
      };
    };

  # Use the systemd-boot EFI boot loader.
  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };

    initrd = {
      availableKernelModules = [
        "xhci_pci"
        "ahci"
        "nvme"
        "usb_storage"
        "usbhid"
        "sd_mod"
      ];
      kernelModules = [ "dm-snapshot" ];
      luks.devices = {
        root = {
          device = "/dev/disk/by-uuid/45940249-dfba-4f19-b038-7fc0dbe58dae";
          preLVM = true;
          allowDiscards = true;
        };
      };
    };

    tmp.useTmpfs = true;
  };

  environment.systemPackages = with pkgs; [
    evremap
    piper
    qFlipper
  ];

  networking = {
    wireless.enable = false;
    hostId = "b356f935";
    hostName = "desktop";
    networkmanager.enable = true;
  };

  hardware = {
    onlykey.enable = true;
    enableRedistributableFirmware = true;
    flipperzero.enable = true;
  };

  services = {
    ratbagd.enable = true;
  };

  fileSystems = {
    "/boot" = {
      device = "/dev/disk/by-uuid/9583-0183";
      fsType = "vfat";
    };

    "/" = {
      device = "/dev/disk/by-uuid/e6364417-8198-4177-bf0f-2583b4db24ae";
      fsType = "ext4";
      options = [
        "noatime"
        "nodiratime"
      ];
    };

    "/home" = {
      device = "/dev/disk/by-uuid/758d4bc1-e041-4491-9c1f-566cea91c436";
      fsType = "ext4";
      options = [
        "noatime"
        "nodiratime"
      ];
    };
  };

  swapDevices = [ { device = "/dev/disk/by-uuid/ee033966-8c2e-4400-abf4-7504c3629f0f"; } ];

  nix.settings.max-jobs = 16;
}
