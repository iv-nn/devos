{ self, ... }:
{
  imports =
    self.nixosSuites.server
    ++ (with self.inputs.nixos-hardware.nixosModules; [
      common-pc-ssd
      common-cpu-intel
    ])
    ++ (with self.users; [
      root.default
      ivann.default
    ]);

  home-manager.users.ivann =
    { ... }:
    {
      imports = self.homeSuites.base;
    };

  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    supportedFilesystems = [ "zfs" ];
    kernelParams = [ "ip=dhcp" ];
    initrd = {
      supportedFilesystems = [ "zfs" ];
      kernelModules = [ "zfs" ];
      luks.devices = {
        root = {
          device = "/dev/disk/by-uuid/cac78736-683a-4853-ba11-60b8ea4a8a3b";
          preLVM = true;
          allowDiscards = true;
        };
      };
      availableKernelModules = [ "r8169" ];
      network = {
        enable = true;
        # FIXME: find a way to start `systemctl default` on login
        ssh = {
          enable = true;
          port = 22222;
        };
      };
      systemd = {
        enable = true;
        # wait for disk being decrypted before import zfs root
        services."zfs-import-root" = {
          after = [ "cryptsetup.target" ];
          wants = [ "cryptsetup.target" ];
        };
      };
    };
    tmp.useTmpfs = true;
  };

  hardware = {
    enableRedistributableFirmware = true;
  };

  stylix = {
    enable = false;
    image = ../white.png;
  };

  networking = {
    hostId = "b02cb074";
    hostName = "parent";
    # nameservers = [ "127.0.0.1" ];
  };

  fileSystems = {
    root = {
      mountPoint = "/";
      device = "rpool/root";
      fsType = "zfs";
    };

    boot = {
      mountPoint = "/boot";
      device = "/dev/disk/by-uuid/5210-06D7";
      fsType = "vfat";
      options = [
        "umask=0077"
        "defaults"
      ];
    };

    # media = {
    #   mountPoint = "/var/media";
    #   device = "storage/media";
    #   fsType = "zfs";
    # };
  };
  swapDevices = [ { device = "/dev/disk/by-uuid/490da449-754e-4efe-b3e6-f6274d5a640b"; } ];
}
