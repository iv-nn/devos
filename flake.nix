{
  description = "Ivann's nix config";

  nixConfig.extra-substituters = [
    "https://nrdxp.cachix.org"
    "https://nix-community.cachix.org"
  ];
  nixConfig.extra-trusted-public-keys = [
    "nrdxp.cachix.org-1:Fc5PSqY2Jm1TrWfm88l6cvGWwz3s93c6IOifQWnhNW4="
    "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
  ];

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "nixpkgs/nixpkgs-unstable";
    nur.url = "github:nix-community/NUR";

    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    stylix = {
      url = "github:danth/stylix/release-24.11";
      inputs.flake-compat.follows = "flake-compat";
      inputs.flake-utils.follows = "flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.home-manager.follows = "home-manager";
    };

    nixos-hardware.url = "github:nixos/nixos-hardware";

    colmena = {
      url = "github:zhaofengli/colmena";
      inputs.flake-compat.follows = "flake-compat";
      inputs.flake-utils.follows = "flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-compat.url = "github:edolstra/flake-compat";
    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs.systems.follows = "nix-systems";
    };
    flake-parts.url = "github:hercules-ci/flake-parts";
    nix-darwin.url = "github:lnl7/nix-darwin";
    nix-systems.url = "github:nix-systems/default";
    haumea = {
      url = "github:nix-community/haumea";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    agenix = {
      url = "github:ryantm/agenix";
      inputs.darwin.follows = "nix-darwin";
      inputs.home-manager.follows = "home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.systems.follows = "nix-systems";
    };
    nvfetcher = {
      url = "github:berberman/nvfetcher";
      inputs.flake-compat.follows = "flake-compat";
      inputs.flake-utils.follows = "flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    devshell = {
      url = "github:numtide/devshell";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    arkenfox = {
      url = "github:dwarfmaster/arkenfox-nixos";
      inputs.flake-compat.follows = "flake-compat";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    emacs-overlay = {
      url = "github:nix-community/emacs-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixvim = {
      url = "github:nix-community/nixvim/nixos-24.11";
      inputs.devshell.follows = "devshell";
      inputs.flake-compat.follows = "flake-compat";
      inputs.flake-parts.follows = "flake-parts";
      inputs.home-manager.follows = "home-manager";
      inputs.nix-darwin.follows = "nix-darwin";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.treefmt-nix.follows = "treefmt-nix";
    };

    authentik-nix = {
      url = "github:nix-community/authentik-nix";
      inputs.flake-compat.follows = "flake-compat";
      inputs.flake-parts.follows = "flake-parts";
      inputs.flake-utils.follows = "flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      nixpkgs-unstable,
      flake-parts,
      colmena,
      haumea,
      home-manager,
      nur,
      agenix,
      nvfetcher,
      arkenfox,
      devshell,
      treefmt-nix,
      stylix,
      emacs-overlay,
      nixos-generators,
      nixvim,
      authentik-nix,
      ...
    }@inputs:
    flake-parts.lib.mkFlake { inherit inputs; } (
      { lib, ... }:
      let
        getDefault = map (x: x.default);
        load =
          src:
          haumea.lib.load {
            inherit src;
            loader = haumea.lib.loaders.path;
          };
      in
      {
        imports = [
          devshell.flakeModule
          treefmt-nix.flakeModule
        ];

        systems = [ "x86_64-linux" ];

        perSystem =
          {
            config,
            pkgs,
            system,
            ...
          }:
          {
            treefmt = {
              projectRootFile = "flake.nix";
              settings.global.excludes = [ "pkgs/_sources/generated.nix" ];
              programs = {
                deadnix.enable = true;
                nixfmt.enable = true;
                shellcheck.enable = true;
                shfmt.enable = true;
              };
            };

            devshells.default = import ./shell {
              inherit config devshell;
              pkgs = pkgs // {
                agenix = agenix.packages.${system}.default;
                colmena = colmena.packages.${system}.colmena;
                nvfetcher = nvfetcher.packages.${system}.default;
              };
            };

            _module.args = {
              pkgs = import nixpkgs {
                inherit system;
                overlays = [ nvfetcher.overlays.default ];
              };
            };
          };

        flake = {
          nixosModules = haumea.lib.load {
            src = ./nixos/modules;
            loader = haumea.lib.loaders.path;
          };

          nixosProfiles = load ./nixos/profiles;

          nixosSuites = with self.nixosProfiles; rec {
            base = getDefault [
              cachix
              core
            ];
            desktop =
              base
              ++ getDefault [
                graphical
                hyprland
              ];
            server = base ++ getDefault [ self.nixosProfiles.server.core ];
          };

          users = load ./nixos/users;

          homeModules = load ./home/modules;

          homeProfiles = load ./home/profiles;

          homeSuites = with self.homeProfiles; {
            base = getDefault [
              core
              shell.zsh
            ];
            dev = getDefault [
              direnv
              fzf
              git
              # editors.emacs
              editors.vim
              modern-unix
              starship
            ];
            desktop = getDefault [
              appearance
              browsers.chromium
              browsers.firefox
              email.thunderbird
              graphical
              kitty
              latex
              ledger
              mpv
              speedcrunch
              trezor
              window-managers.hyprland
              yazi
              waybar
              zathura
            ];
          };

          colmenaHive = colmena.lib.makeHive self.outputs.colmena;

          colmena = {
            meta = {
              nixpkgs = import nixpkgs {
                system = "x86_64-linux";
                config.allowUnfreePredicate =
                  pkg:
                  builtins.elem (lib.getName pkg) [
                    "discord"
                    "exodus"
                    "slack"
                    "steam"
                    "steam-unwrapped"
                    "steam-run"
                    "trezor-suite"
                  ];
                overlays = [
                  nur.overlays.default
                  emacs-overlay.overlay
                ];
              };
              specialArgs = {
                inherit self inputs;
                pkgs-unstable = import nixpkgs-unstable { system = "x86_64-linux"; };
              };
            };

            defaults =
              {
                pkgs,
                pkgs-unstable,
                lib,
                inputs,
                ...
              }:
              {
                imports = [
                  home-manager.nixosModules.home-manager
                  agenix.nixosModules.age
                  stylix.nixosModules.stylix
                  self.nixosModules.nix-path
                  nixos-generators.nixosModules.all-formats
                ];

                home-manager = {
                  useGlobalPkgs = true;
                  useUserPackages = true;
                  sharedModules = [
                    arkenfox.hmModules.arkenfox
                    nixvim.homeManagerModules.nixvim
                    ./home/modules
                  ];
                  extraSpecialArgs = {
                    inherit pkgs-unstable;
                    sources = pkgs.callPackage (import ./pkgs/_sources/generated.nix) { };
                  };
                };

                nix.registry = lib.mapAttrs (_n: flake: { inherit flake; }) inputs;
              };

            desktop = {
              deployment.allowLocalDeployment = true;
              imports = [
                ./hosts/desktop.nix
                ./secrets/hosts/desktop/default.nix
              ];
            };

            laptop = {
              deployment.allowLocalDeployment = true;
              imports = [
                ./hosts/laptop.nix
                ./secrets/hosts/laptop/default.nix
              ];
            };

            work = {
              deployment.allowLocalDeployment = true;
              imports = [
                ./hosts/work.nix
                ./secrets/hosts/work/default.nix
              ];
            };

            server = {
              deployment = {
                targetHost = "192.168.0.100";
                targetPort = 22;
                targetUser = "ivann";
              };
              imports = [
                authentik-nix.nixosModules.default
                ./hosts/server.nix
                ./secrets/hosts/server/default.nix
              ];
            };

            parent = {
              deployment = {
                targetHost = "192.168.0.101";
                targetPort = 22;
                targetUser = "ivann";
              };
              imports = [
                authentik-nix.nixosModules.default
                ./hosts/parent.nix
                ./secrets/hosts/parent/default.nix
              ];
            };
          };
        };
      }
    );
}
