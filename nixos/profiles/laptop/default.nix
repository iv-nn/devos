{ ... }:
{
  services = {
    acpid.enable = true;
    tlp.enable = true;
    thermald.enable = true;

    libinput.touchpad.disableWhileTyping = true;

    logind = {
      extraConfig = ''
        HandlePowerKey=hibernate
        HandleLidSwitch=suspend-then-hibernate
      '';
    };
  };

  programs.light.enable = true;
}
