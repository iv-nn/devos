{ pkgs, ... }:
{
  programs.hyprland.enable = true;

  security.pam.services.hyprlock = { };

  services.greetd = {
    enable = true;
    settings =
      let
        # HACK: bring session vars
        command = pkgs.writeShellScript "startHyprland" ''
          . "/etc/profiles/per-user/ivann/etc/profile.d/hm-session-vars.sh"
          Hyprland
        '';
      in
      {
        initial_session = {
          inherit command;
          user = "ivann";
        };
        default_session = {
          command = "${pkgs.greetd.tuigreet}/bin/tuigreet --cmd ${command}";
        };
      };
  };
}
