{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [ iw ];

  networking = {
    networkmanager = {
      enable = true;
      wifi.macAddress = "random";
    };
  };
}
