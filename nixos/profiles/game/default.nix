{ pkgs, ... }:
{
  programs.steam = {
    enable = true;
    package = pkgs.steam.override { extraLibraries = pkgs: [ pkgs.gperftools ]; };
  };
}
