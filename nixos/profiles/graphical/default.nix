{ pkgs, lib, ... }:
{
  # nixos-generators set this at 10 for install-iso, let him do it to avoid conflict
  boot.loader.timeout = lib.mkDefault 2;

  programs = {
    adb.enable = true;
    firejail.enable = true;
    dconf.enable = true;
    kdeconnect.enable = true;
  };

  security.rtkit.enable = true;

  # Change timezone only on graphical targets
  time.timeZone = "Europe/Paris";

  stylix = {
    enable = true;
    autoEnable = true;
    targets = {
      console.enable = false;
      # TODO: move to home-manager once stylix implement a user level option
      chromium.enable = false;
    };
  };

  services = {
    journald.extraConfig = ''
      SystemMaxUse=1G
    '';

    udisks2.enable = true;

    # It prevent's from using both left and right click at the same times, causing issues in some games
    libinput.mouse.middleEmulation = false;

    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      wireplumber.enable = true;
    };

    udev = {
      extraRules = ''
        # Make udisks2 mount to /media instead of /run/media/user/
        ENV{ID_FS_USAGE}=="filesystem|other|crypto", ENV{UDISKS_FILESYSTEM_SHARED}="1"

        # qmk: allow flashing caterina boards
        SUBSYSTEMS=="usb", ATTRS{idVendor}=="2a03", ATTRS{idProduct}=="0036", TAG+="uaccess", RUN{builtin}+="uaccess", ENV{ID_MM_DEVICE_IGNORE}="1"
        SUBSYSTEMS=="usb", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0036", TAG+="uaccess", RUN{builtin}+="uaccess", ENV{ID_MM_DEVICE_IGNORE}="1"
        SUBSYSTEMS=="usb", ATTRS{idVendor}=="1b4f", ATTRS{idProduct}=="9205", TAG+="uaccess", RUN{builtin}+="uaccess", ENV{ID_MM_DEVICE_IGNORE}="1"
        SUBSYSTEMS=="usb", ATTRS{idVendor}=="1b4f", ATTRS{idProduct}=="9203", TAG+="uaccess", RUN{builtin}+="uaccess", ENV{ID_MM_DEVICE_IGNORE}="1"

        # qmk: allow raw hid access
        SUBSYSTEMS=="usb", ATTRS{idVendor}=="444d", ATTRS{idProduct}=="3536", GROUP="users"
      '';
      packages = [ pkgs.android-udev-rules ];
    };

    trezord.enable = true;
  };

  fileSystems."/media" = {
    fsType = "tmpfs";
    options = [ "size=1M" ];
  };

  virtualisation = {
    docker.enable = true;
    libvirtd.enable = true;
  };
}
