{ lib, ... }:
{
  system.stateVersion = "24.05";

  nix = {
    # Improve nix store disk usage
    gc.automatic = true;
    optimise.automatic = true;

    settings = {
      # Prevents impurities in builds
      sandbox = true;

      # give root and @wheel special privileges with nix
      trusted-users = [
        "root"
        "@wheel"
      ];

      auto-optimise-store = true;

      experimental-features = [
        "flakes"
        "nix-command"
      ];
    };

    # Generally useful nix option defaults
    extraOptions = ''
      min-free = 536870912
      keep-outputs = true
      keep-derivations = true
      fallback = true
    '';
  };

  console = {
    font = "Lat2-Terminus16";
    keyMap = "fr-bepo";
  };
  # US date format is dumb
  i18n.extraLocaleSettings = {
    LC_TIME = "en_GB.UTF-8";
  };

  programs.zsh.enable = true;
  environment.pathsToLink = [ "/share/zsh" ];

  services = {
    openssh = {
      # For rage encryption, all hosts need a ssh key pair
      enable = true;
      openFirewall = lib.mkDefault false;
    };
  };

  networking = {
    firewall = {
      allowedTCPPorts = [ ];
      allowedUDPPorts = [ ];
    };
    nftables.enable = true;
  };
}
