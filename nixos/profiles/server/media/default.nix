{ config, lib, ... }:
{
  services = {
    nginx =
      let
        mkVirtualHost = (import ../utils.nix { inherit lib; }).mkVirtualHost config.networking.domain;
      in
      {
        virtualHosts = mkVirtualHost {
          media = {
            port = 8096;
            locations."/metrics".extraConfig = ''
              deny all;
              return 404;
            '';
          };
          music = {
            port = config.services.navidrome.settings.Port;
            sso = true;
          };
          paperless = {
            port = config.services.paperless.port;
          };
        };
      };

    postgresql = {
      ensureDatabases = [ "paperless" ];
      ensureUsers = [
        {
          name = "paperless";
          ensureDBOwnership = true;
        }
      ];
    };

    # Paper management
    paperless = {
      enable = true;
      settings = {
        PAPERLESS_OCR_LANGUAGE = "fra";
        PAPERLESS_DBHOST = "/run/postgresql";
      };
    };

    # Video streaming
    jellyfin = {
      enable = true;
    };

    # Music streaming
    navidrome = {
      enable = true;
      settings = {
        MusicFolder = "${config.fileSystems.media.mountPoint}/music";
        ReverseProxyWhitelist = "127.0.0.1/32";
        ReverseProxyUserHeader = "X-authentik-username";
        Prometheus.Enabled = true;
      };
    };
  };

  users = {
    groups = {
      document = { };
      media = { };
    };
    users = {
      jellyfin.extraGroups = [
        "media"
        "video"
        "render"
      ];
      navidrome.extraGroups = [ "media" ];
      paperless.extraGroups = [ "document" ];
    };
  };
}
