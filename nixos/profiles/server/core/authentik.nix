{ config, lib, ... }:
{
  services = {
    authentik = {
      enable = true;
      environmentFile = config.age.secrets.authentik.path;
      settings = {
        avatars = "initial";
        disable_startup_analytics = true;
        cert_discovery_dir = "/var/lib/acme/";
      };
    };

    nginx =
      let
        mkVirtualHost = (import ../utils.nix { inherit lib; }).mkVirtualHost config.networking.domain;
      in
      {
        virtualHosts = mkVirtualHost {
          auth = {
            locations."/" = {
              proxyPass = "https://localhost:9443";
              proxyWebsockets = true;
              recommendedProxySettings = true;
            };
          };
        };
      };
  };
}
