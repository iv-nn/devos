{
  config,
  inputs,
  pkgs,
  lib,
  ...
}:
{
  imports = [
    # "${inputs.nixpkgs}/nixos/modules/profiles/headless.nix"
    # "${inputs.nixpkgs}/nixos/modules/profiles/minimal.nix"
    # "${inputs.nixpkgs}/nixos/modules/profiles/hardened.nix"
    ./authentik.nix
    ./dns.nix
    ./monitoring.nix
  ];

  # minimal profile disable Xlibs but we need those
  # environment.noXlibs = false;
  # hardened profile enable apparmor but we have no profiles
  # security.apparmor.enable = false;
  # FIXME: system crash with hardened kernel
  # boot.kernelPackages = pkgs.linuxPackages;
  boot.kernel.sysctl."vm.overcommit_memory" = lib.mkForce 1;

  nix.gc = {
    automatic = true;
    dates = "weekly";
  };

  time.timeZone = "UTC";

  security = {
    acme = {
      acceptTerms = true;
      certs = {
        wildcard = {
          domain = "*.${config.networking.domain}";
          group = config.services.nginx.group;
          dnsProvider = "gandiv5";
          environmentFile = config.age.secrets.acme.path;
        };
      };
    };
    sudo.wheelNeedsPassword = false;
  };

  system.autoUpgrade = {
    enable = true;
    flake = inputs.self.outPath;
    flags = [
      "--update-input"
      "nixpkgs"
      "-L" # print build logs
    ];
    dates = "02:00";
    randomizedDelaySec = "45min";
  };

  assertions = [
    {
      assertion = config.networking.hostName != null;
      message = "Please specify a host name";
    }
    {
      assertion = config.networking.domain != null;
      message = "Please specify a domain";
    }
  ];

  environment.systemPackages = with pkgs; [
    kitty.terminfo
    tmux
  ];

  networking.firewall = {
    allowedTCPPorts = [
      80
      443
    ];
  };

  services = {
    openssh = {
      enable = true;
      openFirewall = true;
      settings = {
        KbdInteractiveAuthentication = false;
        PermitRootLogin = lib.mkForce "no";
        PasswordAuthentication = false;
      };
    };

    nginx = {
      enable = true;
      sslProtocols = "TLSv1.3";
      recommendedOptimisation = true;
      recommendedTlsSettings = true;
      recommendedZstdSettings = true;
      commonHttpConfig =
        let
          proxyTimeout = config.services.nginx.proxyTimeout;
        in
        # We need to manualy specify nix nginx recommended proxy settings
        # without `proxy_redirect off` as it break authentik.
        ''
          proxy_connect_timeout   ${proxyTimeout};
          proxy_send_timeout      ${proxyTimeout};
          proxy_read_timeout      ${proxyTimeout};
          proxy_http_version      1.1;
          # don't let clients close the keep-alive connection to upstream. See the nginx blog for details:
          # https://www.nginx.com/blog/avoiding-top-10-nginx-configuration-mistakes/#no-keepalives
          proxy_set_header        "Connection" "";

          proxy_set_header        Host $host;
          proxy_set_header        X-Real-IP $remote_addr;
          proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header        X-Forwarded-Proto $scheme;
          proxy_set_header        X-Forwarded-Host $host;
          proxy_set_header        X-Forwarded-Server $host;
        '';
    };

    postgresql = {
      enable = true;
      enableJIT = true;
    };

    # opensmtpd = {
    #   enable = true;
    # };

    # File sync
    # syncthing = {
    #   enable = true;
    # };

    ## Backup
    # TODO: setup proton drive
    #
    # restic.backups = {
    #   photo = {
    #     paths = [ "${config.fileSystems.media.mountPoint}/photo" ];
    #   };
    # };

    zfs = {
      autoScrub.enable = true;
      # Don't forget to set `com.sun:auto-snapshot` to true on the datasets
      # autoSnapshot = {
      #   enable = true;
      #   flags = "-k -p --utc";
      # };
    };
  };
}
