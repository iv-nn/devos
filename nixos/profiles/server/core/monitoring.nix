{
  pkgs,
  config,
  lib,
  ...
}:
{

  environment.systemPackages = with pkgs; [ smartmontools ];

  services = {
    blocky.settings = {
      ports.http = 4000;
      prometheus.enable = true;
    };

    nginx =
      let
        mkVirtualHost = (import ../utils.nix { inherit lib; }).mkVirtualHost config.networking.domain;
      in
      {
        statusPage = true;
        virtualHosts = mkVirtualHost {
          scrutiny = {
            port = config.services.scrutiny.settings.web.listen.port;
            sso = true;
          };
          grafana = {
            port = config.services.grafana.settings.server.http_port;
            locations."/" = {
              proxyWebsockets = true;
              recommendedProxySettings = true;
            };
          };
        };
      };

    postgresql = {
      ensureDatabases = [ "grafana" ];
      ensureUsers = [
        {
          name = "grafana";
          ensureDBOwnership = true;
        }
      ];
    };

    # Graphs
    grafana = {
      enable = true;
      settings = {
        analytics = {
          reporting_enabled = false;
          feedback_links_enabled = false;
          check_for_plugin_updates = false;
        };
        database = {
          type = "postgres";
          user = "grafana";
          host = "/run/postgresql";
        };
        server = {
          enable_gzip = true;
          root_url = "https://grafana.${config.networking.domain}";
        };
      };
      provision.datasources.settings.datasources = [
        {
          name = "prometheus";
          type = "prometheus";
          url = "http://localhost:${toString config.services.prometheus.port}";
          jsonData = {
            timeInterval = "1m";
          };
        }
        # {
        #   name = "loki";
        #   type = "loki";
        #   url = "http://localhost:3100";
        # }
      ];
    };

    # Log storage
    # loki = {
    #   enable = true;
    # configuration = {
    #   auth_enabled = false;
    #
    #   server = {
    #     http_listen_port = 3100;
    #     grpc_listen_port = 9096;
    #     log_level = "debug";
    #     grpc_server_max_concurrent_streams = 1000;
    #   };
    #
    #   common = {
    #     instance_addr = "127.0.0.1";
    #     path_prefix = "/var/lib/loki";
    #     storage.filesystem = {
    #       chunks_directory = "/var/lib/loki/chunks";
    #       rules_directory = "/var/lib/loki/rules";
    #     };
    #     replication_factor = 1;
    #     ring.kvstore.store = "inmemory";
    #   };
    #
    #   ingester_rf1.enabled = false;
    #
    #   query_range.results_cache.cache.embedded_cache = {
    #     enabled = true;
    #     max_size_mb = 100;
    #   };
    #
    #   schema_config.configs = [
    #     {
    #       from = "2020-10-24";
    #       store = "tsdb";
    #       object_store = "filesystem";
    #       schema = "v13";
    #       index = {
    #         prefix = "index_";
    #         period = "24h";
    #       };
    #     }
    #   ];
    #
    #   pattern_ingester = {
    #     enabled = true;
    #     metric_aggregation = {
    #       enabled = true;
    #       loki_address = "localhost:3100";
    #     };
    #   };
    #
    #   ruler.alertmanager_url = "http://localhost:9093";
    #   frontend.encoding = "protobuf";
    #   analytics.reporting_enabled = false;
    # };
    # };

    # Log collection
    # alloy = {
    #   enable = true;
    #   extraFlags = [ "--disable-reporting" ];
    # };

    # Metrics storage
    prometheus = {
      enable = true;
      exporters = {
        # nextcloud.enable = true;
        nginx.enable = true;
        node = {
          enable = true;
          enabledCollectors = [
            "logind"
            "systemd"
          ];
          extraFlags = [ "--collector.filesystem.mount-points-exclude=^/(dev|nix|proc|run|sys)($|/)" ];
        };
        smartctl.enable = true;
      };
      scrapeConfigs = [
        {
          job_name = "authentik";
          static_configs = [ { targets = [ "localhost:9300" ]; } ];
        }
        {
          job_name = "blocky";
          static_configs = [ { targets = [ "localhost:4000" ]; } ];
        }
        {
          job_name = "jellyfin";
          static_configs = [ { targets = [ "localhost:8096" ]; } ];
        }
        {
          job_name = "navidrome";
          static_configs = [ { targets = [ "localhost:4533" ]; } ];
        }
        {
          job_name = "node";
          static_configs = [
            { targets = [ "localhost:${toString config.services.prometheus.exporters.node.port}" ]; }
          ];
        }
        {
          job_name = "nginx";
          static_configs = [
            { targets = [ "localhost:${toString config.services.prometheus.exporters.nginx.port}" ]; }
          ];
        }
        {
          job_name = "smartctl";
          static_configs = [
            { targets = [ "localhost:${toString config.services.prometheus.exporters.smartctl.port}" ]; }
          ];
        }
      ];
    };

    # smart
    scrutiny = {
      enable = true;
      collector.enable = true;
    };
  };

  environment.etc."alloy/config.alloy" = {
    text = ''
      local.file_match "local_files" {
        path_targets = [{"__path__" = "/var/log/*.log"}]
        sync_period = "5s"
      }
      loki.source.file "log_scrape" {
        targets    = local.file_match.local_files.targets
        forward_to = [loki.process.filter_logs.receiver]
        tail_from_end = true
      }
      loki.process "filter_logs" {
        stage.drop {
            source = ""
            expression  = ".*Connection closed by authenticating user root"
            drop_counter_reason = "noisy"
          }
        forward_to = [loki.write.grafana_loki.receiver]
      }
      loki.write "grafana_loki" {
        endpoint {
          url = "http://localhost:3100/loki/api/v1/push"
        }
      }
    '';
  };
}
