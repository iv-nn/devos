{ ... }:
{
  networking.firewall = {
    allowedTCPPorts = [ 53 ];
    allowedUDPPorts = [ 53 ];
  };

  # DNS adblock
  services.blocky = {
    enable = true;
    settings = {
      queryLog.type = "none";
      bootstrapDns = [
        { upstream = "https://1.1.1.1/dns-query"; }
        {
          upstream = "https://dns.mullvad.net/dns-query/dns-query";
          ips = [ "194.242.2.2" ];
        }
      ];
      upstreams = {
        groups.default = [
          # AdGuard
          "https://dns.adguard-dns.com/dns-query"
          # Cloudflare
          "https://security.cloudflare-dns.com/dns-query"
          # ControlD
          "https://freedns.controld.com/p2"
          # DNS0.eu
          "https://dns0.eu/dns-query"
          # FDN
          "https://ns0.fdn.fr/dns-query"
          # Mullvad base DNS
          "https://base.dns.mullvad.net/dns-query"
          #NextDNS
          "https://dns.nextdns.io/fea543"
          # Quad9
          "https://dns.quad9.net/dns-query"
        ];
      };
      blocking = {
        clientGroupsBlock = {
          default = [ "default" ];
        };
        # TODO: rename to `denylists` on v0.24
        blackLists = {
          default = [ "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts" ];
        };
      };
    };
  };

}
