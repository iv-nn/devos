{
  config,
  pkgs,
  utils,
  lib,
  ...
}:
{
  environment.systemPackages = with pkgs; [ mediainfo ];

  services = {
    nginx =
      let
        mkVirtualHost = (import ../utils.nix { inherit lib; }).mkVirtualHost config.networking.domain;
      in
      {
        virtualHosts = mkVirtualHost {
          # radarr = {
          #   port = 7878;
          # };
          # sonarr = {
          #   port = 8989;
          # };
          # readarr = {
          #   port = 8787;
          # };
          # lidarr = {
          #   port = 8686;
          # };
          # bazarr = {
          #   port = config.services.bazarr.listenPort;
          # };
          # prowlarr = {
          #   port = 9696;
          # };
          torrent = {
            port = 3300;
            sso = true;
          };
        };
      };

    rtorrent = {
      enable = true;
      downloadDir = "${config.fileSystems.media.mountPoint}/torrent/doing";
      openFirewall = true;
    };

    # flood = {
    #   enable = true;
    # };

    # Movies
    # radarr = {
    #   enable = true;
    # };

    # Series
    # sonarr = {
    #   enable = true;
    # };

    # Ebooks
    # readarr = {
    #   enable = true;
    # };

    # Music
    # lidarr = {
    #   enable = true;
    # };

    # Subtitles
    # bazarr = {
    #   enable = true;
    # };

    # Manager
    # prowlarr = {
    #   enable = true;
    # };
  };

  # TODO: switch to the builtin service when available
  systemd.services.flood =
    let
      port = 3300;
      host = "localhost";
    in
    {
      description = "A modern web UI for various torrent clients.";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      unitConfig = {
        Documentation = "https://github.com/jesec/flood/wiki";
      };
      serviceConfig = {
        Restart = "on-failure";
        RestartSec = "3s";
        ExecStart = utils.escapeSystemdExecArgs ([
          (lib.getExe pkgs.flood)
          "--host"
          host
          "--port"
          (toString port)
          "--rundir=/var/lib/flood"
          "--auth=none"
          "--rtsocket=/run/rtorrent/rpc.sock"
          "--allowedpath=${config.fileSystems.media.mountPoint}"
        ]);

        CapabilityBoundingSet = [ "" ];
        DynamicUser = true;
        Group = config.services.rtorrent.group;
        LockPersonality = true;
        NoNewPrivileges = true;
        PrivateDevices = true;
        PrivateTmp = true;
        ProtectClock = true;
        ProtectControlGroups = true;
        ProtectHome = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        ProtectProc = "invisible";
        ProtectSystem = "strict";
        ReadWritePaths = config.fileSystems.media.mountPoint;
        RestrictAddressFamilies = [
          "AF_UNIX"
          "AF_INET"
          "AF_INET6"
        ];
        RestrictNamespaces = true;
        RestrictRealtime = true;
        RestrictSUIDSGID = true;
        StateDirectory = "flood";
        SystemCallArchitectures = "native";
        SystemCallFilter = [
          "@system-service"
          "@pkey"
          "~@privileged"
        ];
      };
    };
  users = {
    groups = {
      flood = { };
      torrent = { };
    };
    users = {
      rtorrent.extraGroups = [
        "torrent"
        "media"
      ];
      flood = {
        isSystemUser = true;
        group = "flood";
        extraGroups = [
          "torrent"
          "media"
        ];
      };
    };
  };
}
