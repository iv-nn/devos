{ self, pkgs, ... }:
let
  user = "ivann";
in
{
  imports = [ "${self}/secrets/users/${user}/default.nix" ];

  home-manager.users."${user}" = {
    imports = self.homeSuites.base ++ [ "${self}/secrets/users/${user}/home.nix" ];
  };

  users.users."${user}" = {
    isNormalUser = true;
    uid = 1000;
    shell = pkgs.zsh;
    extraGroups = [
      "wheel"
      "networkmanager"
      "audio"
      "video"
      "wireshark"
      "docker"
      "libvirtd"
      "vboxusers"
      "jackaudio"
      "adbusers"
    ];
  };
}
