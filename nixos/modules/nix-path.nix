{ inputs, lib, ... }:
{
  environment.etc = lib.mapAttrs' (name: flake: {
    name = "nix/inputs/${name}";
    value.source = flake.outPath;
  }) inputs;

  nix.nixPath = [ "/etc/nix/inputs" ];
}
