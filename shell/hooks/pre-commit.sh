#!/usr/bin/env bash

if git rev-parse --verify HEAD >/dev/null 2>&1; then
  against=HEAD
else
  # Initial commit: diff against an empty tree object
  against=$(git hash-object -t tree /dev/null)
fi

diff="git diff-index --name-only --cached $against --diff-filter d"

mapfile -t all_files < <($diff)

if ((${#all_files[@]} != 0)); then
  if ! treefmt --fail-on-change "${all_files[@]}"; then
    printf "%b\n" \
      "\nCode was not formated" \
      "Add the formated files and commit" >&2
    exit 1
  fi

  if ! editorconfig-checker -- "${all_files[@]}"; then
    printf "%b\n" \
      "\nCode is not aligned with .editorconfig" \
      "Review the output and commit your fixes" >&2
    exit 1
  fi
fi
