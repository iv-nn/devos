{
  config,
  pkgs,
  devshell,
  ...
}:
let
  hooks = import ./hooks;

  pkgWithCategory = category: package: { inherit package category; };
  linter = pkgWithCategory "linter";
in
{
  imports = [ "${devshell}/extra/git/hooks.nix" ];
  git = {
    inherit hooks;
  };

  packages = with pkgs; [
    agenix
    colmena
    nvfetcher
  ];

  commands = with pkgs; [
    (linter config.treefmt.build.wrapper)
    (linter editorconfig-checker)
    {
      category = "general commands";
      name = "up";
      help = "update the config dependencies";
      command = "nix flake update; cd $PRJ_ROOT/pkgs; ${nvfetcher-bin}/bin/nvfetcher --config ./sources.toml; rm _sources/generated.json";
    }
    {
      category = "general commands";
      name = "bd";
      help = "build all nodes";
      command = "colmena build --experimental-flake-eval";
    }
    {
      category = "general commands";
      name = "sw";
      help = "switch to the new config";
      command = "colmena apply-local --sudo --experimental-flake-eval";
    }
    {
      category = "general commands";
      name = "vm";
      help = "run a config in a vm, usage: vm [host]";
      command = "[[ -n \"\${1:-}\" ]] && nix run .#nixosConfigurations.\"$1\".config.system.build.vm || echo \"No host specified\"";
    }
    {
      category = "general commands";
      name = "gen-iso";
      help = "generate an install iso, usage gen-iso [host]";
      command = "[[ -n \"\${1:-}\" ]] && nix build .#nixosConfigurations.\"$1\".config.formats.install-iso || echo \"No host specified\"";
    }
  ];
}
