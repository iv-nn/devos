{ pkgs, config, ... }:
{
  stylix.targets.waybar.enable = false;

  home.packages = with pkgs; [ playerctl ];

  programs.waybar = {
    enable = true;
    systemd.enable = true;
    settings = {
      mainBar = {
        position = "bottom";
        height = 32;
        margin = "8 0 0";
        modules-left = [
          "hyprland/workspaces"
          "hyprland/window"
          "hyprland/submap"
        ];
        modules-center = [
          "mpris"
        ];
        modules-right = [
          "systemd-failed-units"
          "privacy"
          "network"
          "backlight"
          "battery"
          "cpu"
          "memory"
          "wireplumber"
          "clock"
          "group/systray"
        ];
        "hyprland/workspaces" = {
          format = "{icon}";
          all-outputs = true;
          persistent-workspaces = {
            "1" = [ ];
            "2" = [ ];
            "3" = [ ];
            "4" = [ ];
            "5" = [ ];
            "6" = [ ];
            "7" = [ ];
            "8" = [ ];
            "9" = [ ];
            "10" = [ ];
          };
          move-to-monitor = true;
          on-scroll-up = "${pkgs.hyprland}/bin/hyprctl dispatch workspace e+1";
          on-scroll-down = "${pkgs.hyprland}/bin/hyprctl dispatch workspace e-1";
        };
        "hyprland/window" = {
          format = "{class}";
          icon = true;
          separate-outputs = true;
        };
        backlight = {
          format = "{icon}  {percent:3}%";
          format-icons = [
            "󰛩 "
            "󱩎 "
            "󱩏 "
            "󱩐 "
            "󱩑 "
            "󱩒 "
            "󱩓 "
            "󱩔 "
            "󱩕 "
            "󱩖 "
            "󰛨 "
          ];
          on-scroll-up = "${pkgs.light}/bin/light -A 1";
          on-scroll-down = "${pkgs.light}/bin/light -U 1";
        };
        battery = {
          format = " {icon}  {capacity:3}%";
          format-full = "󱐋{icon}  {capacity:3}%";
          format-charging = "󱐋{icon}  {capacity:3}%";
          format-icons = [
            "󰂎"
            "󰁺"
            "󰁻"
            "󰁼"
            "󰁽"
            "󰁾"
            "󰁿"
            "󰂀"
            "󰂁"
            "󰂂"
            "󰁹"
          ];
        };
        cpu = {
          format = "   {usage:3}%";
        };
        memory = {
          format = "   {percentage:3}%";
        };
        mpris = {
          format = "{player_icon} {dynamic}";
          format-paused = "{status_icon} {dynamic}";
          tooltip-format = "{player} {status} {title} {artits} {album}";
          dynamic-order = [
            "title"
            "artist"
          ];
          format-len = 120;
          player-icons = {
            default = "▶";
          };
          status-icons = {
            paused = "⏸";
          };
          ignored-players = [ "firefox" ];
        };
        network = {
          format-ethernet = " 󰌘 ";
          tooltip-format-ethernet = "{ipaddr}\n󰕒 {bandwidthUpOctets:>}  󰇚 {bandwidthDownOctets:>}";
          format-wifi = "󰖩  {signalStrength:3}%";
          tooltip-format-wifi = "{ipaddr} {essid}\n󰕒 {bandwidthUpOctets:>}  󰇚 {bandwidthDownOctets:>}";
          format-disconnected = " 󰌙 ";
        };
        privacy = { };
        systemd-failed-units = { };
        wireplumber = {
          format = "   {volume:3}%";
          format-muted = " ";
          on-click = "wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle";
        };
        clock = {
          format = "   {:%H:%M}";
          tooltip-format = "<tt><small>{calendar}</small></tt>";
          calendar = {
            mode = "month";
            mode-mon-col = 3;
            weeks-pos = "right";
            on-scroll = 1;
            format = with config.lib.stylix.colors.withHashtag; {
              months = "<span color='${base09}'><b>{}</b></span>";
              days = "<span color='${base05}'><b>{}</b></span>";
              weeks = "<span color='${base03}'><b>W{}</b></span>";
              weekdays = "<span color='${base04}'><b>{}</b></span>";
              today = "<span color='${base0B}'><b>{}</b></span>";
            };
          };
          actions = {
            on-click-right = "mode";
          };
        };
        tray = {
          icon-size = 16;
          spacing = 8;
          show-passive-items = true;
        };
        "group/systray" = {
          orientation = "horizontal";
          drawer = {
            click-to-reveal = true;
          };
          modules = [
            "custom/systray"
            "tray"
          ];
        };
        "custom/systray" = {
          format = "󰅁";
          tooltip = false;
        };
      };
    };

    style = with config.lib.stylix.colors.withHashtag; ''
      * {
        font-family: "Fira Sans";
      }

      tooltip {
        background: ${base01};
        border: 1px solid ${base03};
        border-radius: 8px;
      }

      window#waybar {
        background: transparent;
        font-size: 14px;
      }

      .module {
        background: ${base01};
        border-radius: 8px;
        padding: 0 8px;
        margin: 0 8px;
      }

      .modules-left widget:first-child .module {
        margin-left: 0;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
      }

      .modules-right widget:last-child .module {
        margin-right: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
      }

      #systray .drawer-child .module {
        margin: 0;
        border-radius: 0
      }

      #workspaces button {
        min-height: 22px;
        margin: 4px;
        padding: 0 4px;
        border-radius: 99px;
        color: ${base05};
        background: ${base03};
      }

      #workspaces button.visible,
      #workspaces button.active,
      #workspaces button.urgent {
        color: ${base01};
      }

      #workspaces button.empty {
        background: ${base01};
      }

      #workspaces button.visible {
        background: ${base0D};
      }

      #workspaces button.active {
        background: ${base0B};
      }

      #workspaces button.urgent {
        background: ${base0E};
      }

      window#waybar.empty #window {
        background-color: transparent;
      }

      #submap {
        color: ${base0E};
      }

      #network {
        min-width: 40px;
      }

      #backlight, #battery, #cpu, #memory, #network.wifi, #wireplumber, #clock {
        min-width: 60px;
      }

      #backlight {
        color: ${base06};
      }

      #battery {
        color: ${base07};
      }

      #cpu {
        color: ${base08};
      }

      #memory {
        color: ${base09};
      }

      #network {
        color: ${base0A};
      }

      #privacy {
        color: ${base0B};
      }

      #systemd-failed-units {
        color: ${base0C};
      }

      #wireplumber {
        color: ${base0D};
      }
    '';
  };
}
