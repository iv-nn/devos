{ pkgs, ... }:
let
  bat = "${pkgs.bat}/bin/bat --style numbers --pager never --color always";
  fd = "${pkgs.fd}/bin/fd --follow --color always";
  fd-file = "${fd} --type file";
  fd-directory = "${fd} --type directory";
  file = "${pkgs.file}/bin/file";
  eza-tree = "${pkgs.eza} --tree --level 3 --color always --icons";
in
{
  programs.fzf = {
    enable = true;
    changeDirWidgetCommand = fd-directory;
    changeDirWidgetOptions = [ "--preview '${eza-tree} {}'" ];
    defaultCommand = fd-file;
    defaultOptions = [
      "--height=80%"
      "--ansi"
      "--reverse"
      "--history-size=1000000000"
      "--bind 'ctrl-t:down'"
      "--bind 'ctrl-s:up'"
      "--bind 'ctrl-p:toggle-preview'"
      "--bind 'ctrl-f:page-down'"
      "--bind 'ctrl-b:page-up'"
      "--bind 'ctrl-d:half-page-down'"
      "--bind 'ctrl-u:half-page-up'"
      "--bind 'ctrl-e:jump-accept'"
    ];
    fileWidgetCommand = fd-file;
    fileWidgetOptions = [
      ''
        --preview
          'case \$(${file} --mime {}) in
            *directory*)
              ${eza-tree} {}
              ;;
            *binary*)
              ${file} {}
              ;;
            *)
              ${bat} {}
              ;;
            esac'
      ''
    ];
  };
}
