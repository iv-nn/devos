{
  programs.zathura = {
    enable = true;
    extraConfig = ''
      # jklm →  tsrn
      map t scroll down
      map s scroll up
      map c scroll left
      map r scroll right

      # JK →  TS
      map T navigate next
      map S navigate previous

      # r →  p
      map p rotate rotate-cw

      # R →  u
      map u reload

      # b ↔  a car le a est plus accessible
      map b adjust_window best-fit
      map a adjust_window width

      # Mode Index
      map [index] t navigate_index down
      map [index] s navigate_index up
      map [index] c navigate_index collapse
      map [index] r navigate_index select
      map [index] n navigate_index expand

      map [index] C navigate_index collapse-all
      map [index] N navigate_index expand-all

      map E display_link
      map e follow

      map f toggle_fullscreen
      map [fullscreen] f toggle_fullscreen

      map i toggle_index

      map 9 search
    '';
  };

  xdg = {
    mimeApps.defaultApplications = {
      "application/pdf" = "org.pwmt.zathura.desktop";
    };
  };
}
