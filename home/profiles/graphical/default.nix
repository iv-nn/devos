{ pkgs, pkgs-unstable, ... }:
{
  fonts.fontconfig.enable = true;

  xdg.mimeApps.enable = true;

  home = {
    packages = with pkgs; [
      # dev
      docker-compose
      python3

      # media
      exiftool
      gimp
      guvcview
      imagemagick
      krita
      libreoffice
      mediainfo
      pulsemixer
      pkgs-unstable.yt-dlp

      # misc
      lm_sensors
      wev
      wl-clipboard

      # browser
      tor-browser-bundle-bin

      # security
      nmap
      torsocks

      # misc
      (aspellWithDicts (
        d: with d; [
          en
          fr
        ]
      ))
      glxinfo
      jmtpfs
      libnotify
    ];
  };

  services = {
    blueman-applet.enable = true;
    udiskie.enable = true;
    kdeconnect = {
      enable = true;
      indicator = true;
    };
  };
}
