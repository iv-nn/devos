{ pkgs, ... }:
{
  home.packages = with pkgs; [
    bandwhich
    bingrep
    curlie
    dogdns
    dua
    duf
    du-dust
    eza
    fd
    gping
    hexyl
    procs
    ripgrep
    sd
    tealdeer
    vifm
    vimiv-qt
  ];

  programs = {
    atuin = {
      enable = true;
      settings = {
        dialect = "uk";
        auto_sync = false;
        update_check = false;
        workspaces = true;
      };
    };
    nix-index.enable = true;
    zoxide.enable = true;

    bat.enable = true;

    btop.enable = true;

    broot = {
      enable = true;
      settings = {
        verbs = [
          {
            execution = ":focus";
            key = "r";
          }
          {
            execution = ":parent";
            key = "c";
          }
          {
            execution = ":line_down";
            key = "t";
          }
          {
            execution = ":line_up";
            key = "s";
          }
          {
            execution = ":select_first";
            key = "g";
          }
          {
            execution = ":select_last";
            key = "shift-G";
          }
          {
            execution = ":back";
            key = "u";
          }
          {
            execution = ":page_down";
            key = "ctrl-f";
          }
          {
            execution = ":page_up";
            key = "ctrl-b";
          }
          {
            execution = ":close_panel_ok";
            key = "q";
          }
          {
            execution = ":panel_right";
            key = "ctrl-l";
          }
          {
            execution = ":panel_left";
            key = "ctrl-h";
          }
          {
            execution = ":mode_input";
            key = "i";
          }
          {
            invocation = "edit";
            key = "e";
            execution = "$EDITOR {file}";
            leave_broot = true;
          }
          {
            execution = ":toggle_preview";
            key = "p";
          }
        ];
      };
    };
  };
}
