{ pkgs, ... }:
{
  programs.git = {
    enable = true;

    signing = {
      signByDefault = true;
      key = null;
    };

    extraConfig = {
      init.defaultBranch = "main";
      fetch.prune = true;
      pull.rebase = true;
      push = {
        autoSetupRemote = true;
        useForceIfIncludes = true;
      };
      rebase = {
        autoStash = true;
        autosquash = true;
        updateRefs = true;
      };
    };

    aliases = {
      a = "add --patch";
      co = "checkout";
      cob = "checkout -b";
      f = "fetch --prune";
      c = "commit --verbose";
      p = "push";
      ba = "branch --all";
      bd = "branch --delete";
      bD = "branch --delete --force";
      d = "diff";
      dc = "diff --cached";
      ds = "diff --staged";
      r = "restore";
      rs = "restore --staged";
      s = "status --short --branch";

      # reset
      soft = "reset --soft";
      hard = "reset --hard";
      s1ft = "soft HEAD~1";
      h1rd = "hard HEAD~1";

      # logging
      l = "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit";
      plog = "log --graph --pretty='format:%C(red)%d%C(reset) %C(yellow)%h%C(reset) %ar %C(green)%aN%C(reset) %s'";
      tlog = "log --stat --since='1 Day Ago' --graph --pretty=oneline --abbrev-commit --date=relative";
      rank = "shortlog -sn --no-merges";

      # delete merged branches
      bdm = "!git branch --merged | grep -v '*' | xargs -n 1 git branch -d";

      # checkout a github pr
      pr = "!f() { if [ $# -lt 1 ]; then echo \"Usage: git pr <id> [<remote>] # assuming <remote>[=upstream] is on GitHub\"; else git checkout -q \"$(git rev-parse --verify HEAD)\" && git fetch -fv \"\${2:-upstream}\" pull/\"$1\"/head:pr/\"$1\" && git checkout pr/\"$1\"; fi; }; f";
    };
  };

  home.packages = with pkgs; [ git-crypt ];
}
