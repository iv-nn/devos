{ ... }:
{
  programs.starship = {
    enable = true;
    settings = {
      add_newline = false;
      format = "$directory$character";
      right_format = "$cmd_duration$username$hostname$localip$shlvl$singularity$kubernetes$vcsh$fossil_branch$git_branch$git_commit$git_state$git_metrics$git_status$hg_branch$pijul_channel$docker_context$package$c$cmake$cobol$daml$dart$deno$dotnet$elixir$elm$erlang$fennel$golang$guix_shell$haskell$haxe$helm$java$julia$kotlin$gradle$lua$nim$nodejs$ocaml$opa$perl$php$pulumi$purescript$python$raku$rlang$red$ruby$rust$scala$solidity$swift$terraform$vlang$vagrant$zig$buf$nix_shell$conda$meson$spack$memory_usage$aws$gcloud$openstack$azure$env_var$crystal$custom$sudo$line_break$jobs$battery$time$status$os$container$shell$characte";
      aws = {
        disabled = true;
        symbol = "  ";
        format = "[$symbol]($style)";
      };
      conda = {
        symbol = " ";
        format = "[$symbol]($style)";
      };
      cmd_duration = {
        format = " [$duration]($style) ";
      };
      dart = {
        symbol = " ";
        format = "[$symbol]($style)";
      };
      directory = {
        read_only = " ";
      };
      docker_context = {
        symbol = " ";
        format = "[$symbol]($style)";
      };
      elixir = {
        symbol = " ";
        format = "[$symbol]($style)";
      };
      elm = {
        symbol = " ";
        format = "[$symbol]($style)";
      };
      gcloud = {
        disabled = true;
        symbol = "  ";
        format = "[$symbol]($style)";
      };
      git_branch = {
        symbol = " ";
        format = "[$symbol$branch]($style) ";
      };
      git_status = {
        format = "([「$all_status$ahead_behind」]($style) )";
        conflicted = " ";
        ahead = " \${count} ";
        behind = " \${count}";
        diverged = " ";
        untracked = " ";
        stashed = " ";
        modified = " ";
        staged = " ";
        renamed = " ";
        deleted = " ";
      };
      golang = {
        symbol = " ";
        format = "[$symbol]($style)";
      };
      hg_branch = {
        symbol = " ";
        format = "[$symbol]($style)";
      };
      java = {
        symbol = " ";
        format = "[$symbol]($style)";
      };
      julia = {
        symbol = " ";
        format = "[$symbol]($style)";
      };
      memory_usage = {
        symbol = "󰍛 ";
      };
      nim = {
        symbol = " ";
        format = "[$symbol]($style)";
      };
      nix_shell = {
        symbol = " ";
        format = "[$symbol]($style)";
      };
      nodejs = {
        symbol = " ";
        format = "[$symbol]($style)";
      };
      package = {
        symbol = "󰏗 ";
        format = "[$symbol]($style)";
      };
      python = {
        symbol = "";
        format = "[\${symbol}\${pyenv_prefix}(\${version} )(\\($virtualenv\\) )]($style)";
      };
      rust = {
        symbol = " ";
        format = "[$symbol]($style)";
      };
    };
  };
}
