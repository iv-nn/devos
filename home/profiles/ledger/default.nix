{ config, pkgs, ... }:
{
  home = {
    packages = with pkgs; [
      hledger
      hledger-web
    ];

    sessionVariables = {
      LEDGER_FILE = "\$HOME/finance/unified.journal";
    };

    file."${config.home.homeDirectory}/finance/.keep".text = "";
  };
}
