{ pkgs, ... }:
{
  programs.alot = {
    enable = true;
    bindings = {
      global = {
        s = "move up";
        t = "move down";
        r = "select";
        c = "bclose";
        "ctrl b" = "move page up";
        "ctrl f" = "move page down";
        L = "call hooks.threaded_syncmail(ui)";
        S = "search tag:sent AND NOT tag:killed";
        F = "search tag:flagged";
        "'/'" = "prompt search ";
        "' '" = "";
        "'  b'" = "bufferlist";
        "'  t'" = "taglist";
        "d" = "";
        "dd" = "toggletags deleted";
      };
      envelope = {
        s = "move up";
        t = "move down";
        r = "select";
        c = "bclose";
      };
      search = {
        s = "move up";
        t = "move down";
        u = "toggletags unread";
        r = "untag unread; select; fold *; move last; unfold";
        "','" = "select; reply";
        "';'" = "select; reply --all";
        f = "toggletags flagged";
      };
      thread = {
        s = "move up";
        t = "move down";
        r = "select";
        c = "bclose";
        "'z o'" = "unfold";
        "'z c'" = "fold";
        "'z O'" = "unfold *";
        "'z C'" = "fold *";
        k = "save";
        K = "save --all";
        S = "search tag:sent AND NOT tag:killed";
        "','" = "reply";
        "';'" = "reply --all";
      };
    };
    settings = {
      editor_cmd = "nvim";
      editor_in_thread = true;
      editor_spawn = true;
      history_size = 100000;
      input_timeout = 0.3;
      terminal_cmd = "kitty";
      thread_indent_replies = 4;
      theme = "my_theme";
    };

    tags = {
      main = {
        translated = "";
        normal = "'','','bold,light blue','','bold,light blue',''";
        focus = "'','','bold,light blue','','bold,light blue',''";
      };

      inbox = {
        translated = "";
        normal = "'','','bold,yellow','','bold,yellow',''";
        focus = "'','','bold,yellow','','bold,yellow',''";
      };

      sent = {
        translated = "";
        normal = "'','','bold,yellow','','bold,yellow',''";
        focus = "'','','bold,yellow','','bold,yellow',''";
      };

      junk = {
        translated = "";
        normal = "'','','bold,yellow','','bold,yellow',''";
        focus = "'','','bold,yellow','','bold,yellow',''";
      };

      draft = {
        translated = "";
        normal = "'','','bold,yellow','','bold,yellow',''";
        focus = "'','','bold,yellow','','bold,yellow',''";
      };

      flagged = {
        translated = "";
        normal = "'','','light red','','light red',''";
        focus = "'','','light red','','light red',''";
      };

      unread = {
        translated = "";
      };

      replied = {
        translated = "";
        normal = "'','','bold,yellow','','bold,yellow',''";
        focus = "'','','bold,yellow','','bold,yellow',''";
      };

      lists = {
        translated = "";
        normal = "'','','bold,white','','bold,white',''";
        focus = "'','','bold,white','','bold,white',''";
      };

      attachment = {
        translated = "";
        normal = "'','','bold,light magenta','','bold,light magenta',''";
        focus = "'','','bold,light magenta','','bold,light magenta',''";
      };

      encrypted = {
        translated = "";
        normal = "'','','bold,light green','','bold,light green',''";
        focus = "'','','bold,light green','','bold,light green',''";
      };

      signed = {
        translated = "";
        normal = "'','','bold,dark green','','bold,dark green',''";
        focus = "'','','bold,dark green','','bold,dark green',''";
      };
    };

    hooks = ''
      import alot
      from threading import Thread
      import subprocess


      def _syncmail():
        p = subprocess.Popen(
          "${pkgs.notmuch}/bin/notmuch",
          stdout = subprocess.PIPE,
          stderr = subprocess.PIPE
        )
        p.wait()


      def syncmail(ui):
        if ui:
          notification = ui.notify("fetching email…", timeout = -1)

        _syncmail()

        if ui:
          ui.clear_notify([notification])
          refresh_search_buffers(ui)


      def refresh_search_buffers(ui):
        search_buffers = ui.get_buffers_of_type(alot.buffers.SearchBuffer)
        for buffer in search_buffers:
          buffer.rebuild()


      def threaded_syncmail(*args):
        Thread(target=syncmail, args=args).start()


      def exit():
        _syncmail()


      def loop_hook(ui=None):
        threaded_syncmail(ui)
    '';
  };

  home.file = {
    alotMailcap = {
      source = ./alot_mailcap;
      target = ".mailcap";
    };
  };

  xdg = {
    configFile = {
      alotTheme = {
        source = ./my_theme;
        target = "alot/themes/my_theme";
      };
    };
    dataFile = {
      alotDesktop = {
        source = ./alot.desktop;
        target = "applications/alot.desktop";
      };
    };
  };
}
