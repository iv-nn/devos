{ pkgs, ... }:
{
  programs.astroid = {
    enable = true;
    externalEditor = "kitty nvim %1";
    pollScript = "${pkgs.notmuch}/bin/notmuch";
    extraConfig = {
      startup.queries.sent = "tag:sent";
      editor.attachment_words = "attach,pj,pièce jointe,pièce-jointe,pièces-jointes,ci-joint";
    };
  };

  xdg.configFile = {
    astroidKeybindings = {
      source = ./astroid_keybindings;
      target = "astroid/keybindings";
    };
  };
}
