{ config, lib, ... }:
{
  # home.packages = [pkgs.protonmail-bridge];
  services = {
    protonmail-bridge.enable = true;
  };

  systemd.user.services = {
    pass-secret-service.Service.Environment = lib.mkForce "GNUPGHOME=${config.services.trezor.gpg-agent.configDir}";
    protonmail-bridge = {
      Unit.After = [ "pass-secret-service.Service" ];
    };
  };
}
