{ ... }:
{
  programs.thunderbird = {
    enable = true;
    profiles.ivann = {
      isDefault = true;
      withExternalGnupg = true;
    };
  };
}
