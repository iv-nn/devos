{ ... }:
{
  services.trezor = {
    ssh-agent.enable = true;
    gpg-agent.enable = true;
  };
}
