{ config, pkgs, ... }:
{
  home.packages = with pkgs; [ speedcrunch ];

  xdg.dataFile = {
    speedcrunchTheme = {
      target = "SpeedCrunch/color-schemes/theme.json";
      text =
        with config.lib.stylix.colors.withHashtag;
        builtins.toJSON {
          background = base00;
          cursor = base0E;
          number = base05;
          parens = base09;
          result = base0B;
          comment = base03;
          matched = base0A;
          function = base0D;
          operator = base0C;
          variable = base08;
          scrollbar = base01;
          separator = base03;
          editorbackground = base01;
        };
    };
  };
}
