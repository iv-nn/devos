{ ... }:
{
  programs.kitty = {
    enable = true;
    settings = {
      scrollback_lines = 10000;
      enable_audio_bell = false;
      update_check_interval = 0;
    };
    keybindings = {
      "ctrl+shift+s" = "scroll_line_up";
      "ctrl+shift+t" = "scroll_line_down";
      "ctrl+shift+b" = "scroll_page_up";
      "ctrl+shift+f" = "scroll_page_down";
      "ctrl+shift+p" = "paste_from_selection";
    };
  };

  home = {
    sessionVariables = {
      TERMINAL = "kitty";
    };
  };
}
