{
  config,
  pkgs,
  lib,
  ...
}:
let
  grim = "${pkgs.grim}/bin/grim";
  slurp = "${pkgs.slurp}/bin/slurp";
  satty = "${pkgs.satty}/bin/satty";
  screenshotDir = "~/Pictures/screens";
  screenshotSelect = pkgs.writeShellScript "screenshotSelect" ''${grim} -g "$(${slurp} -c \"#ff0000ff\")" - | ${satty} --filename - --fullscreen --output-filename ${screenshotDir}/$(date "+%Y-%m-%d_%H-%M-%S").png'';
  screenshot = pkgs.writeShellScript "screenshot" ''${grim} -g "$(${slurp} -o -r -c \"#ff0000ff\")" - | ${satty} --filename - --fullscreen --output-filename ${screenshotDir}/$(date "+%Y-%m-%d_%H-%M-%S").png'';
in
{
  home.packages = with pkgs; [
    clipse
    pavucontrol
    udiskie
  ];

  wayland.windowManager.hyprland = {
    enable = true;
    settings = {
      exec-once = [
        "firefox"
        "clipse -listen"
      ];
      general = {
        gaps_out = 8;
        gaps_in = 4;
        border_size = 2;
      };
      decoration = {
        rounding = 5;
      };
      dwindle = {
        preserve_split = true;
      };
      misc = {
        disable_hyprland_logo = true;
        disable_splash_rendering = true;
        focus_on_activate = false;
      };
      monitor = [ ", preferred, auto, 1" ];
      cursor = {
        inactive_timeout = 10;
        no_warps = true;
      };
      input = {
        kb_layout = "fr";
        kb_variant = "bepo";
        follow_mouse = 2;
        numlock_by_default = true;
      };
      binds = {
        workspace_back_and_forth = true;
      };

      "$mod" = "SUPER";
      "$term" = "kitty";
      "$menu" = "wofi --allow-images --no-actions --show drun";

      bind = [
        # start stuff
        "$mod, Return, exec, $term"
        "$mod, o, exec, $menu"

        # lock screen
        "$mod SHIFT, j, exec, hyprlock --immediate"

        # sound control
        "$mod, z, exec, [float] pavucontrol"

        # screenshot
        "$mod SHIFT, k, exec, ${screenshotSelect}"
        "$mod, k, exec, ${screenshot}"

        # paste
        "$mod, v, exec, $term --class clipse -e clipse"

        ## Layout
        "$mod, g, layoutmsg, preselect b"
        "$mod, h, layoutmsg, preselect r"

        ## Windows
        # change focus
        "$mod, TAB, focuscurrentorlast"
        "$mod, c, movefocus, l"
        "$mod, r, movefocus, r"
        "$mod, s, movefocus, u"
        "$mod, t, movefocus, d"
        # move them
        "$mod SHIFT, c, movewindow, l"
        "$mod SHIFT, r, movewindow, r"
        "$mod SHIFT, s, movewindow, u"
        "$mod SHIFT, t, movewindow, d"
        # fullscreen
        "$mod, f, fullscreen, 0"
        # toggle floating
        "$mod SHIFT, f, togglefloating"
        # close
        "$mod SHIFT, x, killactive"

        ## Workspaces
        # select
        "$mod, quotedbl, workspace, 1"
        "$mod, guillemotleft, workspace, 2"
        "$mod, guillemotright, workspace, 3"
        "$mod, parenleft, workspace, 4"
        "$mod, parenright, workspace, 5"
        "$mod, at, workspace, 6"
        "$mod, plus, workspace, 7"
        "$mod, minus, workspace, 8"
        "$mod, slash, workspace, 9"
        "$mod, asterisk, workspace, 10"
        # move
        "$mod SHIFT, quotedbl, movetoworkspace, 1"
        "$mod SHIFT, guillemotleft, movetoworkspace, 2"
        "$mod SHIFT, guillemotright, movetoworkspace, 3"
        "$mod SHIFT, parenleft, movetoworkspace, 4"
        "$mod SHIFT, parenright, movetoworkspace, 5"
        "$mod SHIFT, at, movetoworkspace, 6"
        "$mod SHIFT, plus, movetoworkspace, 7"
        "$mod SHIFT, minus, movetoworkspace, 8"
        "$mod SHIFT, slash, movetoworkspace, 9"
        "$mod SHIFT, asterisk, movetoworkspace, 10"

        ## Monitors
        # change focus
        "$mod, d, focusmonitor, l"
        "$mod, l, focusmonitor, r"
        # Move workspaces
        "$mod SHIFT, d, movecurrentworkspacetomonitor, l"
        "$mod SHIFT, l, movecurrentworkspacetomonitor, r"
      ];
      bindm = [
        # Left button
        "$mod, mouse:272, movewindow"
        # Right button
        "$mod, mouse:273, resizewindow"
      ];
      workspace = [
        # smartgap
        "w[tv1], gapsout:0, gapsin:0"
        "f[1], gapsout:0, gapsin:0"
      ];
      windowrulev2 = [
        ## floating windows
        "float, class:^(org.speedcrunch.)"
        "float, class:^(mpv)"

        ## per workspace windows
        "workspace 1, class:^(firefox)$"
        "workspace 3, class:^(emacs)$"
        "workspace 9, class:^(steam)$"

        # smartgap
        "bordersize 0, floating:0, onworkspace:w[tv1]"
        "rounding 0, floating:0, onworkspace:w[tv1]"
        "bordersize 0, floating:0, onworkspace:f[1]"
        "rounding 0, floating:0, onworkspace:f[1]"

        ## apps
        # clipse
        "float, class:(clipse)"
        "stayfocused, class:(clipse)"
      ];
    };
    extraConfig = ''
      # window resize
      bind = $mod, m, submap, resize

      submap = resize
      binde = , r, resizeactive, 10 0
      binde = , c, resizeactive, -10 0
      binde = , t, resizeactive, 0 -10
      binde = , s, resizeactive, 0 10
      bind = , escape, submap, reset
      submap = reset
    '';
  };

  programs = {
    hyprlock = {
      enable = true;
      settings = with config.lib.stylix.colors; {
        general = {
          grace = 30;
        };
        background = {
          monitor = "";
          color = "rgb(${base01})";
          path = lib.mkForce "";
        };
        input-field = {
          monitor = "";
          size = "200, 50";
          hide_input = true;
        };
      };
    };
    wofi.enable = true;
  };

  services = {
    hypridle = {
      enable = true;
      settings = {
        general = {
          # avoid starting multiple hyprlock instances.
          lock_cmd = "pidof hyprlock || hyprlock";
          # lock before suspend.
          before_sleep_cmd = "loginctl lock-session";
          # to avoid having to press a key twice to turn on the display.
          after_sleep_cmd = "hyprctl dispatch dpms on";
        };
        listener = [
          # lock screen
          {
            timeout = 300;
            on-timeout = "loginctl lock-session";
          }
          # turn screen off
          {
            timeout = 330;
            on-timeout = "hyprctl dispatch dpms off";
            on-resume = "hyprctl dispatch dpms on";
          }
        ];
      };
    };

    hyprpaper.enable = true;

    mako = {
      enable = true;
      borderRadius = 5;
      defaultTimeout = 30000;
      margin = "20";
    };

    gammastep = {
      enable = lib.mkDefault true;
      tray = true;
      settings = {
        general = {
          adjustment-method = "wayland";
        };
      };
    };
  };
}
