{ config, pkgs, ... }:
{
  stylix = {
    base16Scheme = "${pkgs.base16-schemes}/share/themes/catppuccin-frappe.yaml";

    fonts = {
      serif = config.stylix.fonts.sansSerif;

      sansSerif = {
        name = "Fira Sans";
        package = pkgs.fira;
      };

      monospace = {
        name = "Fira Code Nerd Font";
        package = pkgs.nerdfonts.override { fonts = [ "FiraCode" ]; };
      };

      emoji = {
        name = "Noto Color Emoji";
        package = pkgs.noto-fonts-emoji;
      };
    };
  };
}
