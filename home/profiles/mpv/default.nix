{ pkgs, ... }:
{
  programs.mpv = {
    enable = true;
    bindings = {
      c = "seek -5";
      r = "seek +5";
      t = "seek -60";
      s = "seek +60";
      k = "screenshot";
      K = "screenshot video";
    };
    config = {
      osc = false;
    };
    scripts = with pkgs.mpvScripts; [
      mpris
      sponsorblock
      thumbnail
    ];
  };
}
