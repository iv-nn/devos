{ pkgs, ... }:
{
  programs.firefox = {
    enable = true;

    package = pkgs.firefox.override { nativeMessagingHosts = [ pkgs.tridactyl-native ]; };

    arkenfox = {
      enable = true;
      version = "133.0";
    };

    profiles.ivann = {
      extensions = with pkgs.nur.repos.rycee.firefox-addons; [
        bitwarden
        (bypass-paywalls-clean.override (
          let
            version = "4.0.5.0";
          in
          {
            inherit version;
            url = "https://gitflic.ru/project/magnolia1234/bpc_uploads/blob/raw?file=bypass_paywalls_clean-${version}.xpi";
            sha256 = "sha256-vVo7KlKlQwWt5a3y2ff3zWFl8Yc9duh/jr4TC5sa0Y4=";
          }
        ))
        canvasblocker
        consent-o-matic
        french-dictionary
        multi-account-containers
        sidebery
        sponsorblock
        tridactyl
        ublock-origin
      ];

      arkenfox = {
        enable = true;
        "0000".enable = true;
        "0100" = {
          enable = true;
          # Restore previous session
          "0102"."browser.startup.page".value = 3;
        };
        "0200".enable = true;
        "0300".enable = true;
        "0400".enable = true;
        "0600" = {
          enable = true;
          "0610"."browser.send_pings".enable = true;
        };
        "0700".enable = true;
        "0800".enable = true;
        "0900".enable = true;
        "1000" = {
          enable = true;
          # Enable disk cache for performance reasons
          "1001"."browser.cache.disk.enable".value = true;
        };
        "1200".enable = true;
        "1600".enable = true;
        "1700".enable = true;
        "2000".enable = true;
        "2400".enable = true;
        "2600" = {
          enable = true;
          # Keep pdfjs
          "2620".enable = false;
        };
        "2700".enable = true;
        "2800" = {
          enable = true;
          # Keep cache and history
          "2811"."privacy.clearOnShutdown.cache".value = false;
          "2811"."privacy.clearOnShutdown.history".value = false;
          "2811"."privacy.clearOnShutdown_v2.cache".value = false;
          "2811"."privacy.clearOnShutdown_v2.historyFormDataAndDownloads".value = false;
        };
        "5000" = {
          enable = true;
          # Never remember passwords
          "5003"."signon.rememberSignons".enable = true;
        };
        "5500" = {
          # Disable DRM
          "5508"."media.eme.enabled".value = false;
          "5508"."browser.eme.ui.enabled".value = false;
        };
        "9000".enable = true;
      };

      settings = {
        "toolkit.legacyUserProfileCustomizations.stylesheets" = true;
        "media.autoplay.default" = 5;
        "identity.fxaccounts.enabled" = false;
        "browser.uidensity" = "compact";
        "dom.private-attribution.submission.enabled" = false;
        # Disable history navigation via horizontal scroll
        "widget.disable-swipe-tracker" = true;
      };

      userChrome = ''
        #TabsToolbar {
          display: none;
        }

        #sidebar-header {
          display: none;
        }
      '';
    };
  };

  xdg = {
    configFile =
      let
        googleImageUrl = "https://lens.google.com/uploadbyurl?url=";
        searchEngineSelectors = "a, input, textarea, [role=button]";
      in
      {
        "tridactyl/tridactylrc".text = ''
          " This wipes all existing settings. This means that if a setting in this file
          " is removed, then it will return to default. In other words, this file serves
          " as an enforced single point of truth for Tridactyl's configuration.
          sanitize tridactyllocal tridactylsync

          " General Settings
          colorscheme shydactyl

          set update.nag false

          set hintchars auiectsrnbpowvdljmhfgqkxy
          set hintuppercase false

          set editorcmd kitty --class floatingAppFocus -- nvim -c "nnoremap <ESC><ESC> :q!<ENTER> | nnoremap <ENTER> :x<ENTER> | set filetype=markdown | :startinsert" %f

          set incsearch true

          set followpagepatterns.next ^(next|newer|suivant)\b|»|>>|more
          set followpagepatterns.prev ^(prev(ious)?|older|précédent)\b|«|<<

          " Binds
          bind c scrollpx -50
          bind r scrollpx 50

          bind t scrollline 5
          bind s scrollline -5

          bind C back
          bind R forward

          bind ,c followpage prev
          bind ,r followpage next

          bind T tabnext
          bind S tabprev

          bind j fillcmdline tabopen
          bind J current_url tabopen

          bind l reload
          bind L reloadhard

          bind e hint
          bind E hint -b
          bind gE hint -qb

          unbind f
          unbind F

          bind ,k hint -k
          bind ,w hint -JW mpvsafe
          bind ,i js tri.excmds.hint("-Jc",tri.excmds.INPUTTAGS_selectors)


          bind ,m hint -JFc img e => {tri.excmds.open("${googleImageUrl}" + e.src);}
          bind ,M hint -JFc img e => {tri.excmds.tabopen("${googleImageUrl}" + e.src);}

          bind ,p hint -p
          bind ,y hint -y
          bind ,v hint -h

          alias editor_rm composite editor " | jsb -p tri.native.run(`rm -f "''${JS_ARG[0]}"`)
          bind --mode=insert <C-i> editor_rm
          bind --mode=input <C-i> editor_rm

          bindurl youtu(\.be|be\.com) e hint -J
          bindurl youtu(\.be|be\.com) E hint -Jb
          bindurl youtu(\.be|be\.com) gE hint -Jqb

          bindurl google(\.[a-zA-Z0-9]+){1,2}/search e hint -Jc ${searchEngineSelectors}
          bindurl google(\.[a-zA-Z0-9]+){1,2}/search E hint -Jbc ${searchEngineSelectors}
          bindurl google(\.[a-zA-Z0-9]+){1,2}/search gE hint -Jqbc ${searchEngineSelectors}

          bindurl ^https://duckduckgo.com e hint -Jc ${searchEngineSelectors}
          bindurl ^https://duckduckgo.com E hint -Jbc ${searchEngineSelectors}
          bindurl ^https://duckduckgo.com gE hint -Jqb ${searchEngineSelectors}

          bind ,t hint -Jc [class*="expand"],[class="togg"],[class="comment_folder"]

          bind ,<Space> nohlsearch

          bind --mode=visual c js document.getSelection().modify("extend","backward","character")
          bind --mode=visual r js document.getSelection().modify("extend","forward","character")

          bind --mode=visual t js document.getSelection().modify("extend","forward","line")
          bind --mode=visual s js document.getSelection().modify("extend","backward","line")

          bind --mode=visual k composite js document.getSelection().toString() | fillcmdline open search
          bind --mode=visual K composite js document.getSelection().toString() | fillcmdline tabopen search

          " Search engines

          set searchengine duckduckgo

          set searchurls.ddg https://duckduckgo.com/?q=
          set searchurls.w https://en.wikipedia.org/wiki/Special:Search?search=%s&go=Go&ns0=1
          set searchurls.aw https://wiki.archlinux.org/?search=
          set searchurls.nw https://nixos.wiki/index.php?search=
          set searchurls.g https://www.google.com/search?hl=en&q=
          set searchurls.u https://www.urbandictionary.com/define.php?term=

          " Containers
          containercreate personal blue fingerprint
          containercreate work turquoise briefcase
          containercreate banking green dollar
          containercreate shopping yellow cart
          containercreate chill red vacation
          containercreate reddit orange chill
          containercreate sharing purple circle
          containercreate private pink fence

          " Autocmds
          autocmd FullscreenEnter .* mode ignore

          autocmd FullscreenEnter .* set modeindicator false
          autocmd FullscreenLeft .* set modeindicator true

          # autocmd DocLoad .* no_mouse_mode
        '';
      };

    mimeApps.defaultApplications = {
      "application/x-extension-htm" = "firefox.desktop";
      "application/x-extension-html" = "firefox.desktop";
      "text/html" = "firefox.desktop";
      "x-scheme-handler/http" = "firefox.desktop";
      "x-scheme-handler/https" = "firefox.desktop";
    };
  };
}
