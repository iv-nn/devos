{ ... }:
{
  programs.qutebrowser = {
    enable = true;
    searchEngines = {
      DEFAULT = "https://duckduckgo.com/?q={}";
      w = "https://en.wikipedia.org/wiki/Special:Search?search={}&go=Go&ns0=1";
      aw = "https://wiki.archlinux.org/?search={}";
      nw = "https://nixos.wiki/index.php?search={}";
      g = "https://www.google.com/search?hl=en&q={}";
    };
    settings = {
      auto_save.session = true;
      session.lazy_restore = true;
      tabs = {
        show = "switching";
        show_switching_delay = 1000;
        position = "left";
        background = true;
      };
      content = {
        default_encoding = "utf-8";
        webrtc_ip_handling_policy = "default-public-interface-only";
        canvas_reading = false;
        javascript.can_open_tabs_automatically = true;
      };
      downloads = {
        position = "bottom";
        remove_finished = 0;
      };
      editor.command = [
        "emacs"
        "--name=emacs-browser"
        "{}"
      ];
      hints.chars = "auiectsrnbpovdljmhfgqkxy";
      spellcheck.languages = [
        "en-GB"
        "fr-FR"
      ];
    };
    keyBindings = {
      normal = {
        c = "scroll left";
        r = "scroll right";
        t = "scroll down";
        s = "scroll up";
        C = "back";
        R = "forward";
        T = "tab-next";
        S = "tab-prev";
        gt = "tab-move +";
        gs = "tab-move -";
        gn = "set-cmd-text -s :buffer";
        "èc" = "back --tab";
        "èr" = "forward --tab";
        "È" = "config-cycle tabs.show always switching";
        hd = "download-clear";
        ho = "tab-only";
        J = "tab-focus";
        ",b" = "open qute://bookmarks#bookmarks";
        ",h" = "open qute://history";
        ",q" = "open qute://bookmarks";
        ",s" = "open qute://settings";
        ",d" = "devtools bottom";
        f = "fullscreen";
        e = "hint";
        E = "hint all tab";
        we = "hint all window";
        l = "reload";
        L = "reload --force";
        F = "yank selection --sel;; later 10 open --tab --related {primary}";
        ",l" = ''config-cycle spellcheck.languages ["en-GB"] ["fr-FR"] ["en-GB", "fr-FR"]'';
        ",a" = ''spawn --userscript qute-pass --username-pattern "login: (.+)" --username-target "secret"'';
        ",u" = ''spawn --userscript qute-pass --username-pattern "login: (.+)" --username-target "secret" --username-only'';
        ",p" = ''spawn --userscript qute-pass --username-pattern "login: (.+)" --username-target "secret" --password-only'';
      };
      caret = {
        C = "scroll left";
        R = "scroll right";
        T = "scroll down";
        S = "scroll up";
        c = "move-to-prev-char";
        r = "move-to-next-char";
        t = "move-to-next-line";
        s = "move-to-prev-line";
        F = "yank selection --sel;; later 10 open --tab --related {primary}";
      };
    };
    extraConfig = ''
      c.colors.webpage.bg = 'white'
    '';
  };
}
