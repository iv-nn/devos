{ pkgs, ... }:
let
  mkFishPlugin = p: {
    name = p.pname;
    src = p.src;
  };
in
{
  programs = {
    fish = {
      enable = true;
      plugins =
        with pkgs.fishPlugins;
        map mkFishPlugin [
          autopair
          done
          puffer
        ];
      shellAliases = {
        hs = "history | grep";
        psg = "ps aux | grep";
        l = "exa -l --git --icons";
        lt = "l --tree";
        ll = "l --sort modified";
      };
      shellInit = ''
        # Disable greeting
        set -g fish_greeting

        set -gx LS_COLORS "${
          builtins.readFile (pkgs.runCommand "dircolors" { } "${pkgs.vivid}/bin/vivid generate ayu > $out")
        }"

        set -gx MANPAGER 'less -R --use-color -Dd+r -Du+b'
        set -gx MANROFFOPT '-P -c'

        # Bindings
        function fish_user_key_bindings
          bind \ec prevd-or-backward-word
          bind \er nextd-or-forward-word
          bind \cz 'fg 2>/dev/null; commandline -f repaint'
        end

        # Set ayu Light theme
        set -g fish_color_autosuggestion 8A9199
        set -g fish_color_cancel -r
        set -g fish_color_command 55B4D4
        set -g fish_color_comment ABB0B6
        set -g fish_color_cwd 399EE6
        set -g fish_color_cwd_root red
        set -g fish_color_end ED9366
        set -g fish_color_error F51818
        set -g fish_color_escape 4CBF99
        set -g fish_color_history_current --bold
        set -g fish_color_host normal
        set -g fish_color_host_remote yellow
        set -g fish_color_keyword 55B4D4
        set -g fish_color_match F07171
        set -g fish_color_normal 575F66
        set -g fish_color_operator FF9940
        set -g fish_color_option 575F66
        set -g fish_color_param 575F66
        set -g fish_color_quote 86B300
        set -g fish_color_redirection A37ACC
        set -g fish_color_search_match --background=FF9940
        set -g fish_color_selection --background=FF9940
        set -g fish_color_status red
        set -g fish_color_user brgreen
        set -g fish_color_valid_path --underline
        set -g fish_pager_color_background
        set -g fish_pager_color_completion normal
        set -g fish_pager_color_description 'B3A06D'  'yellow'
        set -g fish_pager_color_prefix 'normal'  '--bold'  '--underline'
        set -g fish_pager_color_progress 'brwhite'  '--background=cyan'
        set -g fish_pager_color_secondary_background
        set -g fish_pager_color_secondary_completion
        set -g fish_pager_color_secondary_description
        set -g fish_pager_color_secondary_prefix
        set -g fish_pager_color_selected_background --background=FF9940
        set -g fish_pager_color_selected_completion
        set -g fish_pager_color_selected_description
        set -g fish_pager_color_selected_prefix
      '';
    };
  };
}
