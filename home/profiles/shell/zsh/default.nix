{ config, pkgs, ... }:
{
  home = {
    packages = with pkgs; [
      eza
      jq
      libnotify
    ];

    sessionPath = [ "$HOME/.local/bin" ];

    sessionVariables = {
      MANPAGER = "${pkgs.less}/bin/less -R --use-color -Dd+r -Du+b";
      MANROFFOPT = "-P -c";
    };
  };

  programs = {
    zsh = {
      enable = true;
      autosuggestion.enable = true;
      enableCompletion = true;
      syntaxHighlighting.enable = true;
      defaultKeymap = "viins";
      dotDir = ".config/zsh";
      history = {
        expireDuplicatesFirst = true;
        extended = true;
        ignoreDups = true;
        ignoreSpace = true;
        share = true;
        path = "${config.xdg.dataHome}/zsh/history";
        size = 1000000000;
      };
      initExtraBeforeCompInit = ''
        bgnotify_threshold=0

        function bgnotify_formatted { ## args: (exit_status, command, elapsed_seconds)
          elapsed="$(( $3 % 60 ))s"
          (( $3 >= 60 )) && elapsed="$((( $3 % 3600) / 60 ))m $elapsed"
          (( $3 >= 3600 )) && elapsed="$(( $3 / 3600 ))h $elapsed"
          if [ $1 -eq 0 ]; then
            notify-send --urgency=normal "success  $elapsed" "$2"
          else
            notify-send --urgency=critical "error  $elapsed" "$2"
          fi
        }
      '';
      initExtra = ''
        ZSH_CACHE_DIR=$HOME/.cache/zsh
        [ -d "$ZSH_CACHE_DIR" ] || mkdir -p "$ZSH_CACHE_DIR"

        unalias run-help
        autoload -Uz run-help
        autoload -Uz run-help-git
        autoload -Uz run-help-ip
        autoload -Uz run-help-openssl
        autoload -Uz run-help-sudo
        autoload -Uz edit-command-line
        autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
        autoload -Uz colors
        colors

        ### Options

        ## Changing Directories

        # cd = pushd
        setopt AUTO_PUSHD

        # make pushd silent
        setopt PUSHD_SILENT

        # blank pushd goes to home
        setopt PUSHD_TO_HOME


        ## Completion

        setopt COMPLETE_IN_WORD


        ## Expansion and Globbing

        # allow #, ~ and ^ in filename patterns
        setopt EXTENDED_GLOB

        # don't throw an error when no match are found
        setopt NO_NOMATCH

        # sort numeric filenames numerically
        setopt NUMERIC_GLOB_SORT

        ## History

        # Remove superfluous blanks before recording entry
        setopt HIST_REDUCE_BLANKS

        ## Input/Output

        # spell check commands
        setopt CORRECT

        # disable flow control (^s and ^q)
        setopt NO_FLOW_CONTROL

        ## Job Control

        # report background and suspended jobs before exiting a shell
        setopt CHECK_JOBS

        ## Zle

        # disable beeps
        setopt NO_BEEP


        ### Completion

        zstyle ':completion:*' accept-exact '*(N)'
        # completion caching, rehash to clear
        zstyle ':completion:*' use-cache on
        zstyle ':completion:*' cache-path "$ZSH_CACHE_DIR/completion"

        # menu if nb items > 2
        zstyle ':completion:*' menu select=2

        # hilight ambiguous character
        zstyle ':completion:*' show-ambiguity

        # colors
        zstyle -e ':completion:*:default' list-colors 'reply=("''${PREFIX:+=(#bi)($PREFIX:t)(?)*==34=34}:''${(s.:.)LS_COLORS}")';

        # completers
        zstyle ':completion:*' completer _complete _match _approximate

        # Increase the number of errors based on the length of the typed word. But make
        # sure to cap (at 7) the max-errors to avoid hanging.
        zstyle ':completion:*:approximate:*' max-errors 1 numeric
        zstyle -e ':completion:*:approximate:*' max-errors 'reply=($((($#PREFIX+$#SUFFIX)/3>7?7:($#PREFIX+$#SUFFIX)/3))numeric)'

        # ignore what's already on the line
        zstyle ':completion:*:(rm|kill|killall|diff|cp|mv):*' ignore-line yes

        zstyle ':completion:*' ignore-parents parents pwd

        zstyle ':completion:*' list-separator '┆ '

        zstyle ':completion:*' verbose yes
        zstyle ':completion:*:descriptions' format '%F{yellow}%B── %d ──%b%f'
        zstyle ':completion:*:messages' format '%F{red}%d%f'
        zstyle ':completion:*' group-name '''
        zstyle ':completion:*:manuals' separate-sections true

        zstyle ':completion:*:processes' command 'ps -u $LOGNAME -o pid,user,command -w'
        zstyle ':completion:*:*:kill:*:processes' list-colors "=(#b) #([0-9]#)*=29=34"

        # Don't complete uninteresting users...
        zstyle ':completion:*:*:*:users' ignored-patterns \
        adm amanda apache avahi beaglidx bin cacti canna clamav daemon dbus \
        distcache dovecot fax ftp games gdm gkrellmd gopher hacluster \
        haldaemon halt hsqldb ident junkbust ldap lightdm lp mail mailman \
        mailnull messagebus mldonkey mysql nagios named netdump news nfsnobody \
        'nixbld*' nm-iodine nm-openvpn nobody nscd ntp nut nx openvpn operator \
        pcap polkituser postfix postgres privoxy pulse pvm qemu-libvirtd \
        quagga radvd rpc rpcuser rpm rtkit shutdown squid sshd sync \
        systemd-coredump systemd-journal-gateway systemd-network systemd-oom \
        systemd-resolve systemd-timesync trezord uucp vcsa xfs '_*' \

        # ... unless we really want to.
        zstyle '*' single-ignored show

        zstyle :compinstall filename "$HOME/.config/zsh/.zshrc"


        ### Binding

        zle -N edit-command-line
        zle -N up-line-or-beginning-search
        zle -N down-line-or-beginning-search

        ## bind special keys
        # https://wiki.archlinux.org/index.php/Zsh#Key_bindings

        # create a zkbd compatible hash;
        # to add other keys to this hash, see: man 5 terminfo
        typeset -A key

        key[Home]=''${terminfo[khome]}
        key[End]=''${terminfo[kend]}
        key[Insert]=''${terminfo[kich1]}
        key[Delete]=''${terminfo[kdch1]}
        key[Up]=''${terminfo[kcuu1]}
        key[Down]=''${terminfo[kcud1]}
        key[Left]=''${terminfo[kcub1]}
        key[Right]=''${terminfo[kcuf1]}
        key[PageUp]=''${terminfo[kpp]}
        key[PageDown]=''${terminfo[knp]}
        key[STab]=''${terminfo[kcbt]}

        # setup key accordingly
        [[ -n "''${key[Home]}"     ]]  && bindkey -- "''${key[Home]}"     beginning-of-line
        [[ -n "''${key[End]}"      ]]  && bindkey -- "''${key[End]}"      end-of-line
        [[ -n "''${key[Insert]}"   ]]  && bindkey -- "''${key[Insert]}"   overwrite-mode
        [[ -n "''${key[Delete]}"   ]]  && bindkey -- "''${key[Delete]}"   delete-char
        [[ -n "''${key[Up]}"       ]]  && bindkey -- "''${key[Up]}"       up-line-or-beginning-search
        [[ -n "''${key[Down]}"     ]]  && bindkey -- "''${key[Down]}"     down-line-or-beginning-search
        [[ -n "''${key[Left]}"     ]]  && bindkey -- "''${key[Left]}"     backward-char
        [[ -n "''${key[Right]}"    ]]  && bindkey -- "''${key[Right]}"    forward-char
        [[ -n "''${key[PageUp]}"   ]]  && bindkey -- "''${key[PageUp]}"   beginning-of-buffer-or-history
        [[ -n "''${key[PageDown]}" ]]  && bindkey -- "''${key[PageDown]}" end-of-buffer-or-history
        [[ -n "''${key[STab]}"     ]]  && bindkey -- "''${key[STab]}"     reverse-menu-complete

        # Finally, make sure the terminal is in application mode, when zle is
        # active. Only then are the values from $terminfo valid.
        if (( ''${+terminfo[smkx]} && ''${+terminfo[rmkx]} )); then
          autoload -Uz add-zle-hook-widget
          function zle_application_mode_start { echoti smkx }
          function zle_application_mode_stop { echoti rmkx }
          add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
          add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
        fi

        bindkey ' ' magic-space

        bindkey -M viins '^w' backward-kill-word

        # edit current command line in vim with CTRL-f
        bindkey -M vicmd '^e' edit-command-line
        bindkey -M viins '^e' edit-command-line

        # open help with CTRL-h
        bindkey -M viins '^h'   run-help
        bindkey -M vicmd '^h'   run-help

        # bepo
        bindkey -M vicmd '*'    beginning-of-line
        bindkey -M vicmd 's'    history-beginning-search-backward
        bindkey -M vicmd 't'    history-beginning-search-forward
        bindkey -M vicmd 'c'    backward-char
        bindkey -M vicmd 'r'    forward-char


        ### Misc

        compinit -d "$ZSH_CACHE_DIR/zcompdump"

        nixify() {
          if [ ! -e ./.envrc ]; then
            echo "use nix" > .envrc
            direnv allow
          fi
          if [ ! -e shell.nix ]; then
            cat > shell.nix <<'EOF'
        with import <nixpkgs> {};

        mkShell {
          buildInputs = [
          ];
        }
        EOF
            ''${EDITOR:-vim} shell.nix
          fi
        }
      '';
      localVariables = {
        KEYTIMEOUT = 1;
        ZSH_HIGHLIGHT_HIGHLIGHTERS = [
          "main"
          "brackets"
        ];
        LS_COLORS = builtins.readFile (
          pkgs.runCommand "dircolors" { } ''${pkgs.vivid}/bin/vivid generate one-dark > $out''
        );
      };
      plugins = [
        {
          name = "zsh-completions";
          src = pkgs.zsh-completions.src;
        }
        {
          name = "zsh-autopair";
          src = pkgs.zsh-autopair.src;
        }
        {
          name = "bgnotify";
          src = ./plugins;
        }
        {
          name = "dirpersist";
          src = pkgs.oh-my-zsh.src + /plugins/dirpersist;
        }
        {
          name = "fancy-ctrl-z";
          src = pkgs.oh-my-zsh.src + /plugins/fancy-ctrl-z;
        }
      ];
      shellAliases = {
        hs = "history | grep";
        psg = "ps aux | grep";
        l = "exa -l --git --icons";
        lt = "l --tree";
        ll = "l --sort modified";
        ls = "ls --color=auto";
        grep = "grep --color=auto";
        wifi = "nmcli -ask device wifi";
      };
    };
  };
}
