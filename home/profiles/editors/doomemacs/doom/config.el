(setq! doom-font (font-spec :family "FiraCode Nerd Font Mono" :size 16)
       doom-variable-pitch-font (font-spec :family "FiraCode Nerd Font" :size 16)
       doom-localleader-key ","
       doom-theme 'doom-ayu-light
       which-key-idle-delay 0.1
       display-line-numbers-type 'relative)

;; (setq-default indent-tabs-mode nil
;;               tab-width 2)
;; (setq indent-line-function 'insert-tab)

(map! :map evil-treemacs-state-map
      "r" #'treemacs-RET-action
      "c" #'treemacs-COLLAPSE-action
      "R" #'treemacs-visit-node-ace
      "C" #'treemacs-collapse-parent-node)

(use-package! treemacs
  :defer t
  :init
  (setq! +treemacs-git-mode 'deferred)
  (map! :desc "Select Treemacs" :nv "M-*" #'treemacs-select-window)
  :config
  (setq! treemacs-show-hidden-files nil
         treemacs-project-follow-mode t))

(use-package! lsp-nix
  :after (lsp-mode)
  :demand t
  :custom
  (lsp-nix-nil-formatter ["nixfmt"]))

(after! lsp-mode (setq lsp-enable-symbol-highlighting nil))

;; (use-package! web-mode
;;   :defer
;;   :custom
;;   (web-mode-markup-indent-offset 2)
;;   (web-mode-css-indent-offset 2)
;;   (web-mode-code-indent-offset 2))

(after! ispell
  (setq ispell-program-name "hunspell"
        ispell-dictionary "en_US,fr-classique")
  ;; ispell-set-spellchecker-params has to be called
  ;; before ispell-hunspell-add-multi-dic will work
  (ispell-set-spellchecker-params)
  (ispell-hunspell-add-multi-dic "en_US,fr-classique"))

;; For saving words to the personal dictionary, don't infer it from
;; the locale, otherwise it would save to ~/.hunspell_en_US.
(setq ispell-personal-dictionary "~/.hunspell_personal")
;; The personal dictionary file has to exist, otherwise hunspell will
;; silently not use it.
(unless (file-exists-p ispell-personal-dictionary)
  (write-region "" nil ispell-personal-dictionary))
