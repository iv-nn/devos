{
  config,
  lib,
  pkgs,
  sources,
  ...
}:
let
  emacs = pkgs.emacs29;
in
{
  programs.emacs = {
    enable = true;
    package = emacs;
  };

  services.emacs = {
    enable = true;
    package = emacs;
    client.enable = true;
  };

  home = {
    activation.installDoomEmacs =
      let
        emacsConfig = "${config.xdg.configHome}/emacs";
        doomScript = "${emacsConfig}/bin/doom";
      in
      lib.hm.dag.entryAfter [ "writeBoundary" ] ''
        ${pkgs.rsync}/bin/rsync -a --chmod=D755,F744 "${sources.doomemacs.src}/" "${emacsConfig}"

        if [[ -x "${doomScript}" ]]; then
          ${doomScript} sync
        fi
      '';

    packages = with pkgs; [
      # required dependencies
      git
      (ripgrep.override { withPCRE2 = true; })
      gnutls

      # optional dependencies
      coreutils
      fd
      clang

      # :checkers spell
      (hunspellWithDicts (
        with hunspellDicts;
        [
          fr-classique
          en-us-large
        ]
      ))

      # :ui treemacs
      python3

      # :tools editorconfig
      editorconfig-core-c

      # :tools lsp
      unzip

      # :lang javascript
      nodePackages.eslint

      # :lang markdown
      pandoc

      # :lang nix
      nixfmt-rfc-style
      nil

      # :lang org
      graphviz
      sqlite

      # :lang sh
      nodePackages.bash-language-server
      shellcheck
      shfmt

      # :lang yaml
      nodePackages.yaml-language-server

      # :lang web
      nodePackages.prettier
    ];

    shellAliases = {
      e = "emacsclient --reuse-frame --no-wait";
    };
  };

  xdg.configFile.doom.source = ./doom;
}
