{ ... }:
{
  programs.helix = {
    enable = true;
    settings = {
      keys = {
        normal = {
          "c" = "move_char_left";
          "t" = "move_line_down";
          "s" = "move_line_up";
          "r" = "move_char_right";

          "h" = "replace"; # r
          "H" = "replace_with_yanked"; # R
          "j" = "find_till_char"; # t
          "J" = "till_prev_char"; # T
          "k" = "select_regex"; # s
          "A-k" = "split_selection_on_newline"; # A-s
          "K" = "split_selection"; # S
          "l" = "change_selection"; # c
          "A-l" = "change_selection_noyank"; # A-c
          "L" = "copy_selection_on_next_line"; # C
          "A-C" = "copy_selection_on_prev_line"; # A-C
        };

        select = {
          "c" = "extend_char_left";
          "t" = "extend_line_down";
          "s" = "extend_line_up";
          "r" = "extend_char_right";

          "j" = "extend_till_char";
          "T" = "extend_till_prev_char";
        };
      };
    };
  };
}
