{
  pkgs,
  pkgs-unstable,
  sources,
  ...
}:
{
  stylix.targets.nixvim.enable = false;

  programs.nixvim = {
    enable = true;
    defaultEditor = true;
    vimAlias = true;
    vimdiffAlias = true;
    colorschemes.catppuccin = {
      enable = true;
      settings = {
        flavour = "frappe";
        integrations = {
          indent_blankline = {
            enabled = true;
            scope_color = "surface1";
            colored_indent_levels = true;
          };
          lsp_trouble = true;
          neotest = true;
          noice = true;
          window_picker = true;
          which_key = true;
        };
      };
    };
    opts = {
      termguicolors = true;
      number = true;
      relativenumber = true;
      showmode = false;
      title = true;
      mouse = "a";
      ignorecase = true;
      smartcase = true;
      wrap = true;
      breakindent = true;
      tabstop = 2;
      shiftwidth = 2;
      expandtab = true;
      shiftround = true;
      undofile = true;
      updatetime = 100;
      cursorline = true;
      signcolumn = "yes";
      scrolloff = 8;
      sidescrolloff = 8;
      foldmethod = "expr";
      foldexpr = "nvim_treesitter#foldexpr()";
      foldenable = false;
      showmatch = true;
      showcmd = false;
      wildmenu = true;
      completeopt = [
        "menu"
        "menuone"
        "noselect"
      ];
      splitbelow = true;
      splitright = true;
      timeoutlen = 500;
    };
    globals = {
      mapleader = " ";
      maplocalleader = ",";
    };
    clipboard = {
      register = "unnamedplus";
      providers.wl-copy.enable = true;
    };
    autoCmd = [
      {
        desc = "Open file on the same line it was closed";
        callback.__raw = ''
          function ()
            if vim.fn.line("'\"") > 1 and vim.fn.line("'\"") <= vim.fn.line("$") then
              vim.api.nvim_exec("normal! g'\"", false)
            end
          end
        '';
        event = [ "BufReadPost" ];
        pattern = [ "*" ];
      }
    ];
    userCommands = {
      ToggleSpell = {
        desc = "Toggle spell checking";
        command.__raw = ''
          function ()
            vim.o.spell = not (vim.o.spell)
          end
        '';
      };
    };
    keymaps = [
      {
        mode = "n";
        key = "è";
        action.__raw = "require('flash').jump";
        options.desc = "Jump using flash";
      }
      # Top level
      {
        mode = "n";
        key = "<leader><Tab>";
        action = ":edit #<cr>";
        options.desc = "Switch to last buffer";
      }
      {
        mode = "n";
        key = "<leader> ";
        action.__raw = "require('telescope.builtin').git_files";
        options.desc = "Find git files";
      }
      {
        mode = "n";
        key = "<leader>.";
        action.__raw = "require('telescope.builtin').find_files";
        options.desc = "Find files";
      }
      {
        mode = "n";
        key = "<leader>/";
        action.__raw = "require('telescope.builtin').live_grep";
        options.desc = "Search for string";
      }
      {
        mode = "n";
        key = "<leader>:";
        action.__raw = "require('telescope.builtin').commands";
        options.desc = "Search commands";
      }
      {
        mode = "n";
        key = "<leader>u";
        action.__raw = "require('telescope').extensions.undo.undo";
        options.desc = "Search undos";
      }

      # Local
      {
        mode = "n";
        key = "<localleader>a";
        action.__raw = "vim.lsp.buf.code_action";
        options.desc = "Code action";
      }
      {
        mode = "n";
        key = "<localleader>k";
        action.__raw = "vim.lsp.buf.hover";
        options.desc = "Display hover information";
      }
      {
        mode = "n";
        key = "<localleader>j";
        action.__raw = "vim.lsp.buf.rename";
        options.desc = "Rename all references";
      }
      {
        mode = "n";
        key = "<localleader>t";
        action.__raw = "vim.diagnostic.goto_next";
        options.desc = "Next diagnostic";
      }
      {
        mode = "n";
        key = "<localleader>s";
        action.__raw = "vim.diagnostic.goto_prev";
        options.desc = "Previous diagnostic";
      }
      {
        mode = "n";
        key = "<localleader>l";
        action.__raw = "vim.diagnostic.open_float";
        options.desc = "Display diagnostic";
      }
      {
        mode = "n";
        key = "<localleader>f";
        action = ":Format<cr>";
        options.desc = "Format";
      }

      # Buffer
      {
        mode = "n";
        key = "<leader>bb";
        action.__raw = "require('telescope.builtin').buffers";
        options.desc = "Find buffers";
      }

      # Code
      {
        mode = "n";
        key = "<leader>ca";
        action.__raw = "vim.lsp.buf.code_action";
        options.desc = "Code action";
      }
      {
        mode = "n";
        key = "<leader>ck";
        action.__raw = "vim.lsp.buf.hover";
        options.desc = "Display hover information";
      }
      {
        mode = "n";
        key = "<leader>cs";
        action.__raw = "vim.lsp.buf.signature_help";
        options.desc = "Display signature information";
      }
      {
        mode = "n";
        key = "<leader>cj";
        action.__raw = "vim.lsp.buf.rename";
        options.desc = "Rename all references";
      }
      {
        mode = "n";
        key = "<leader>ci";
        action.__raw = "require('telescope.builtin').lsp_implementations";
        options.desc = "List implementations";
      }
      {
        mode = "n";
        key = "<leader>cd";
        action.__raw = "require('telescope.builtin').lsp_definitions";
        options.desc = "List definitions";
      }
      {
        mode = "n";
        key = "<leader>ct";
        action.__raw = "require('telescope.builtin').lsp_type_definitions";
        options.desc = "List type definitions";
      }
      {
        mode = "n";
        key = "<leader>cx";
        action = ":Trouble diagnostics toggle focus=false filter.buf=0<cr>";
        options.desc = "List file diagnostics";
      }
      {
        mode = "n";
        key = "<leader>cX";
        action = ":Trouble diagnostics toggle<cr>";
        options.desc = "List workspace diagnostics";
      }

      # Find
      {
        mode = "n";
        key = "<leader>ff";
        action.__raw = "require('telescope.builtin').find_files";
        options.desc = "Find buffers";
      }
      {
        mode = "n";
        key = "<leader>fr";
        action.__raw = "require('telescope.builtin').oldfiles";
        options.desc = "Find buffers";
      }
      {
        mode = "n";
        key = "<leader>fl";
        action.__raw = "require('telescope.builtin').loclist";
        options.desc = "Find loc list";
      }
      {
        mode = "n";
        key = "<leader>fr";
        action.__raw = "require('telescope.builtin').jumplist";
        options.desc = "Find jumplist";
      }
      {
        mode = "n";
        key = "<leader>fp";
        action.__raw = "require('telescope').extensions.projects.projects";
        options.desc = "Find projects";
      }

      # Git
      {
        mode = "n";
        key = "<leader>gg";
        action.__raw = "require('neogit').open";
        options.desc = "Git status";
      }
      {
        mode = "n";
        key = "<leader>gl";
        action.__raw = "require('telescope.builtin').git_commits";
        options.desc = "List git commits";
      }
      {
        mode = "n";
        key = "<leader>gb";
        action.__raw = "function () require('gitsigns').blame_line({full=true}) end";
        options.desc = "Show git blame";
      }

      # Help
      {
        mode = "n";
        key = "<leader>hc";
        action.__raw = "require('telescope.builtin').commands";
        options.desc = "Find commands";
      }
      {
        mode = "n";
        key = "<leader>hv";
        action.__raw = "require('telescope.builtin').vim_options";
        options.desc = "Find commands";
      }
      {
        mode = "n";
        key = "<leader>hk";
        action.__raw = "require('telescope.builtin').keymaps";
        options.desc = "Find commands";
      }
      {
        mode = "n";
        key = "<leader>hh";
        action.__raw = "require('telescope.builtin').help_tags";
        options.desc = "Find commands";
      }

      # Open
      {
        mode = "n";
        key = "<leader>ot";
        action = ":NvimTreeFindFile<cr>";
        options.desc = "Open file tree on current file";
      }

      # Toggle
      {
        mode = "n";
        key = "<leader>js";
        action = ":ToggleSpell<cr>";
        options.desc = "Toggle spell check";
      }
      {
        mode = "n";
        key = "<leader>jt";
        action = ":NvimTreeToggle<cr>";
        options.desc = "Toggle file tree";
      }
      {
        mode = "n";
        key = "<leader>jb";
        action.__raw = "require('gitsigns').toggle_current_line_blame";
        options.desc = "Toggle git blame";
      }
      {
        mode = "n";
        key = "<leader>jf";
        action = ":FormatToggle<cr>";
        options.desc = "Toggle auto format";
      }

      # Window
      {
        mode = "n";
        key = "<leader>ww";
        action.__raw = ''
          function()
            local picked_window_id = require('window-picker').pick_window() or vim.api.nvim_get_current_win()
            vim.api.nvim_set_current_win(picked_window_id)
          end
        '';
        options.desc = "Select window";
      }
    ];
    plugins = {
      # TODO: investate these plugins
      #?? align-nvim
      #?? git-conflict
      #?? gitlinker
      #?? hardtime
      #?? hmts
      #?? image
      #?? inc-rename
      #?? lsp-lines
      #?? ltex_extra
      #?? mkdnflow
      #?? neoscroll
      #?? nvim-surround
      #?? nvim-ufo
      #?? octo
      #?? precognition
      #?? qmk
      auto-session.enable = true;
      cmp = {
        enable = true;
        settings = {
          experimental.ghost_text = true;
          sources = [
            { name = "nvim_lsp"; }
            # TODO: configure locality bonus comparator
            { name = "buffer"; }
            { name = "path"; }
            { name = "npm"; }
            { name = "luasnip"; }
          ];
          mapping = {
            "<C-b" = "cmp.mapping.scroll_docs(-4)";
            "<C-f" = "cmp.mapping.scroll_docs(4)";
            "<CR>" = "cmp.mapping.confirm({ select = true })";
            "<Tab>" = ''
              cmp.mapping(function(fallback)
                    if cmp.visible() then
                      cmp.select_next_item()
                    elseif require('luasnip').expand_or_jumpable() then
                      require('luasnip').expand_or_jump()
                    elseif has_words_before() then
                      cmp.complete()
                    else
                      fallback()
                    end
                  end, { "i", "s" })
            '';
            "<S-Tab>" = ''
              cmp.mapping(function(fallback)
                    if cmp.visible() then
                      cmp.select_prev_item()
                    elseif require('luasnip').jumpable(-1) then
                      require('luasnip').jump(-1)
                    else
                      fallback()
                    end
                  end, { "i", "s" })'';
          };
          snippet.expand = ''
            function(args)
                require('luasnip').lsp_expand(args.body)
              end
          '';
        };
        cmdline = {
          "/" = {
            mapping.__raw = "cmp.mapping.preset.cmdline()";
            sources = [ { name = "buffer"; } ];
          };
          ":" = {
            mapping.__raw = "cmp.mapping.preset.cmdline()";
            sources = [
              { name = "path"; }
              {
                name = "cmdline";
                options.ignore_cmds = [
                  "Man"
                  "!"
                ];
              }
            ];
          };
        };
      };
      dap = {
        enable = true;
        extensions.dap-ui.enable = true;
      };
      debugprint.enable = true;
      diffview.enable = true;
      flash = {
        enable = true;
        settings = {
          labels = "etisuran,c";
          modes.char.enabled = false;
        };
      };
      friendly-snippets.enable = true;
      gitsigns.enable = true;
      indent-blankline.enable = true;
      lsp = {
        enable = true;
        capabilities = "require('cmp_nvim_lsp').default_capabilities()";
        servers = {
          bashls.enable = true;
          cssls.enable = true;
          dockerls.enable = true;
          elixirls.enable = true;
          eslint = {
            enable = true;
            filetypes = [
              "javascript"
              "javascriptreact"
              "javascript.jsx"
              "typescript"
              "typescriptreact"
              "typescript.tsx"
              "vue"
              "svelte"
              "astro"
              "json"
              "graphql"
            ];
            settings.workingDirectory.mode = "auto";
          };
          gleam = {
            enable = true;
            package = pkgs-unstable.gleam;
          };
          graphql.enable = true;
          html.enable = true;
          jsonls.enable = true;
          lua_ls = {
            enable = true;
            settings = {
              telemetry.enable = false;
            };
          };
          marksman.enable = true;
          nil_ls = {
            enable = true;
            settings.formatting.command = [ "nixfmt" ];
          };
          pylsp = {
            enable = true;
            settings = {
              plugins = {
                ruff.enabled = true;
              };
            };
          };
          rust_analyzer = {
            enable = true;
            settings.check.command = "clippy";
            installCargo = false;
            installRustc = false;
          };
          yamlls.enable = true;
        };
      };
      lsp-format = {
        enable = true;
        settings = {
          json = {
            exclude = [ "jsonls" ];
          };
        };
      };
      lspkind = {
        enable = true;
        mode = "symbol";
      };
      lualine = {
        enable = true;
        settings = {
          options.theme = "catppuccin";
          extensions = [
            "nvim-dap-ui"
            "nvim-tree"
            "trouble"
          ];
          sections = {
            lualine_a = [
              {
                __unkeyed-1 = "mode";
                fmt = "trunc(80, 1, 0, true, 16)";
              }
            ];
            lualine_b = [
              {
                __unkeyed-1 = "branch";
                fmt = "trunc(96, 8, 32, false, 32)";
              }
              "diff"
              "diagnostics"
            ];
            lualine_x = [
              {
                __unkeyed-1 = "filetype";
                icon_only = true;
              }
            ];
          };
        };
      };
      luasnip.enable = true;
      markdown-preview.enable = true;
      navbuddy = {
        enable = true;
        lsp.autoAttach = true;
        mappings = {
          "<esc>" = "close";
          "q" = "close";
          "t" = "next_sibling";
          "s" = "previous_sibling";

          "c" = "parent";
          "r" = "children";
          "0" = "root";

          "v" = "visual_name";
          "V" = "visual_scope";

          "y" = "yank_name";
          "Y" = "yank_scope";

          "i" = "insert_name";
          "I" = "insert_scope";

          "a" = "append_name";
          "A" = "append_scope";

          "j" = "rename";

          "d" = "delete";

          "f" = "fold_create";
          "F" = "fold_delete";

          "C" = "comment";

          "<enter>" = "select";
          "o" = "select";
          "J" = "move_down";
          "K" = "move_up";

          "p" = "toggle_preview";

          "<C-v>" = "vsplit";
          "<C-s>" = "hsplit";
        };
      };
      navic = {
        enable = true;
        settings.lsp.auto_attach = true;
      };
      neogit = {
        enable = true;
        # TODO: got back to release version once mappings is fixed
        package = pkgs-unstable.vimPlugins.neogit;
        settings = {
          graph_style = "unicode";
          mappings = {
            popup = {
              "r" = false;
              "j" = "RebasePopup";
              "t" = false;
            };
            status = {
              "s" = false;
              "S" = false;
              "<c-s>" = false;
              "k" = "Stage";
              "K" = "StageUnstaged";
              "<c-k>" = "StageAll";
            };
          };
        };
      };
      # neorg.enable = true;
      neotest = {
        enable = true;
        adapters = {
          jest.enable = true;
          vitest.enable = true;
        };
      };
      noice = {
        enable = true;
        settings.presets.bottom_search = true;
      };
      none-ls = {
        enable = true;
        sources.formatting = {
          prettier = {
            enable = true;
            # settings.extra_filetypes = [ "graphql" ];
          };
          purs_tidy = {
            enable = true;
            package = null;
          };
        };
      };
      nvim-autopairs.enable = true;
      nvim-colorizer.enable = true;
      nvim-tree = {
        enable = true;
        disableNetrw = true;
        actions.windowPicker.picker.__raw = "require('window-picker').pick_window";
        onAttach.__raw = ''
          function (bufnr)
            local tree = require("nvim-tree.api")
            local function opts(desc)
              return { desc = "nvim-tree: " .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
            end

            -- default mappings
            tree.config.mappings.default_on_attach(bufnr)

            -- custom mappings
            vim.keymap.del('n', 's', { buffer = bufnr})
            vim.keymap.del('n', 'c', { buffer = bufnr})
            vim.keymap.set('n', 'r', tree.node.open.edit, opts('Open'))
            vim.keymap.set('n', 'h', tree.node.open.horizontal, opts('Open horizontal split'))
            vim.keymap.set('n', 'v', tree.node.open.vertical, opts('Open vertical split'))
          end
        '';
      };
      parinfer-rust.enable = true;
      project-nvim.enable = true;
      rainbow-delimiters.enable = true;
      schemastore.enable = true;
      telescope = {
        enable = true;
        extensions = {
          fzf-native.enable = true;
          ui-select.enable = true;
          undo.enable = true;
        };
        enabledExtensions = [ "projects" ];
      };
      todo-comments.enable = true;
      treesitter = {
        enable = true;
        settings = {
          highlight.enable = true;
          indent.enable = true;
        };
      };
      treesitter-context = {
        enable = true;
        settings.max_lines = 3;
      };
      treesitter-textobjects.enable = true;
      trouble.enable = true;
      ts-autotag.enable = true;
      # TODO: redo config when using neovim 0.10
      ts-context-commentstring = {
        enable = true;
        extraOptions.enable_autocmd = false;
      };
      typescript-tools = {
        enable = true;
        settings.settings.expose_as_code_action = [
          "fix_all"
          "add_missing_imports"
          "remove_unused"
          "remove_unused_imports"
        ];
      };
      web-devicons.enable = true;
      which-key = {
        enable = true;
        settings = {
          spec = [
            {
              __unkeyed = "<leader>b";
              group = "buffer";
            }
            {
              __unkeyed = "<leader>c";
              group = "code";
            }
            {
              __unkeyed = "<leader>f";
              group = "find";
            }
            {
              __unkeyed = "<leader>g";
              group = "git";
            }
            {
              __unkeyed = "<leader>h";
              group = "help";
            }
            {
              __unkeyed = "<leader>j";
              group = "toggle";
            }
            {
              __unkeyed = "<leader>o";
              group = "open";
            }
            {
              __unkeyed = "<leader>w";
              group = "window";
            }
          ];
          triggers_black_list = {
            n = [
              "ca"
              "ci"
            ];
          };
        };
      };
      wtf.enable = true;
    };
    extraPlugins = with pkgs.vimPlugins; [
      (pkgs.vimUtils.buildVimPlugin {
        name = "bepo-nvim";
        src = sources.bepo-nvim.src;
      })
      bigfile-nvim
      boole-nvim
      (pkgs.vimUtils.buildVimPlugin {
        name = "hlsearch-nvim";
        src = sources.hlsearch-nvim.src;
      })
      nvim-window-picker
    ];
    extraConfigLuaPre = ''
      --- @param trunc_width number trunctates component when screen width is less then trunc_width
      --- @param trunc_len number truncates component to trunc_len number of chars
      --- @param hide_width number hides component when window width is smaller then hide_width
      --- @param no_ellipsis boolean whether to disable adding '...' at end after truncation
      --- @param max_len number always truncates to this length
      --- return function that can format the component accordingly
      local function trunc(trunc_width, trunc_len, hide_width, no_ellipsis, max_len)
        return function(str)
          local win_width = vim.o.columns
          if hide_width ~= 0 and win_width < hide_width then
            return '''
          elseif trunc_width and trunc_len and win_width < trunc_width and #str > trunc_len then
            return str:sub(1, trunc_len) .. (no_ellipsis and ''' or '…')
          end
          return str:sub(1, max_len) .. ((no_ellipsis or #str < max_len) and ''' or '…')
        end
      end

      local has_words_before = function()
        local line, col = unpack(vim.api.nvim_win_get_cursor(0))
        return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match('%s') == nil
      end

      require('bepo').setup()
    '';

    extraConfigLua = ''
      require('bigfile').setup({
        filesize = 1
      })

      require('boole').setup({
        mappings = {
          increment = '<C-a>',
          decrement = '<C-x>',
        }
      })

      require('hlsearch').setup()

      require('window-picker').setup({
        selection_chars = 'etisuran,c',
        picker_config = {
          statusline_winbar_picker = {
            use_winbar = "always"
          }
        }
      })

      local cmp = require('cmp')
      local cmp_autopairs = require('nvim-autopairs.completion.cmp')

      -- insert '(' after function or method
      cmp.event:on(
        'confirm_done',
        cmp_autopairs.on_confirm_done()
      )

      local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
      for type, icon in pairs(signs) do
        local hl = "DiagnosticSign" .. type
        vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
      end

      local get_option = vim.filetype.get_option
      vim.filetype.get_option = function(filetype, option)
        return option == "commentstring"
        and require("ts_context_commentstring.internal").calculate_commentstring()
        or get_option(filetype, option)
      end
    '';
    extraLuaPackages = ps: [ ps.lua-utils-nvim ];
    extraPackages = with pkgs; [
      fd
      ripgrep
      tree-sitter

      # language tool in LaTeX, markdown, git commits…
      # ltex-ls

      # nix
      nixfmt-rfc-style

      # python
      (python3.withPackages (
        p:
        (with p; [
          python-lsp-ruff
          python-lsp-server
        ])
      ))

      # js/ts
      nodePackages.prettier

      # shell
      shellcheck
      shfmt
    ];
  };
}
