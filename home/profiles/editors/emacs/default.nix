{ pkgs, ... }:
let
  emacs = pkgs.emacs29-pgtk;
  config = ./config.org;
in
{
  stylix.targets.emacs.enable = false;

  programs.emacs = {
    enable = true;
    # client.enable = true;
    package = (
      pkgs.emacsWithPackagesFromUsePackage {
        inherit config;
        package = emacs;
        alwaysEnsure = true;
        alwaysTangle = true;
        extraEmacsPackages = epkgs: (with epkgs; [ treesit-grammars.with-all-grammars ]);
        override = _final: prev: {
          lsp-mode = prev.melpaPackages.lsp-mode.overrideAttrs (_old: {
            LSP_USE_PLISTS = true;
          });
        };
      }
    );
  };

  home = {
    packages = with pkgs; [

      # required dependencies
      git
      parinfer-rust
      (ripgrep.override { withPCRE2 = true; })
      gnutls
      unzip

      # optional dependencies
      coreutils
      fd
      clang

      # :checkers spell
      # enchant
      # nuspell
      hunspell
      hunspellDicts.fr-classique
      hunspellDicts.en-us-large

      # :ui treemacs
      python3

      # :tools editorconfig
      editorconfig-core-c

      # :tools lsp
      emacs-lsp-booster

      # :lang js/ts
      nodejs_20
      nodePackages.typescript-language-server
      nodePackages.typescript
      nodePackages.eslint
      nodePackages.prettier
      vscode-langservers-extracted

      # :lang graphql
      # nodePackages.graphql-language-service-cli

      # :lang markdown
      marksman
      pandoc

      # :lang nix
      nixfmt-rfc-style
      nil

      # :lang org
      graphviz
      sqlite
      zip

      # :lang python
      # (python3.withPackages (
      #   p:
      #   (with p; [
      #     python-lsp-server
      #     python-lsp-black
      #     python-lsp-ruff
      #   ])
      # ))

      # :lang rust
      # TODO

      # :lang sh
      nodePackages.bash-language-server
      shellcheck
      shfmt

      # :lang yaml
      yaml-language-server

      # :lang web
      vscode-langservers-extracted
    ];

    shellAliases = {
      e = "emacsclient --reuse-frame --no-wait";
    };
  };

  xdg.configFile.emacs.source =
    (pkgs.runCommand "tangle-config" { } ''
      cp ${config} config.org
      ${emacs}/bin/emacs -Q --batch ./config.org -f org-babel-tangle
      mkdir $out
      mv early-init.el init.el $out
    '').out;
}
