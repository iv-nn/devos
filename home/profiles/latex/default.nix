{ ... }:
{
  programs.texlive = {
    enable = true;
    extraPackages = tpkgs: {
      inherit (tpkgs)
        scheme-basic
        collection-luatex
        collection-langfrench
        latexmk
        ;
    };
  };
}
