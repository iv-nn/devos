{ pkgs, ... }:
{
  home.packages = with pkgs; [
    calibre
    digikam
    discord
    element-desktop
    # exodus
    gramps
    josm
    lmms
    mktorrent
    signal-desktop
    sox
    xournalpp
  ];
}
