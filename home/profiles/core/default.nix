{ pkgs, ... }:
{
  home = {
    stateVersion = "24.05";

    packages = with pkgs; [
      file
      gnupg
      lsof
      ncdu
      p7zip
      pciutils
      sshfs
      usbutils
    ];
  };

  programs.readline = {
    enable = true;
    variables = {
      editing-mode = "vi";
      show-mode-in-prompt = true;
      vi-cmd-mode-string = "N ";
      vi-ins-mode-string = "I ";
      show-all-if-ambiguous = true;

      colored-stats = true;
      mark-symlinked-directories = true;
      menu-complete-display-prefix = true;
    };
  };
}
