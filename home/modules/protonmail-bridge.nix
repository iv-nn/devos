{
  config,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.services.protonmail-bridge;
in
with lib;
{
  options = {
    services.protonmail-bridge = {
      enable = mkEnableOption { name = "Protonmail Bridge"; };
    };
  };

  config = mkIf cfg.enable {
    home.packages = [ pkgs.protonmail-bridge ];

    services.pass-secret-service.enable = true;

    systemd.user.services.protonmail-bridge = {
      Unit = {
        Description = "Protonmail Bridge";
        After = [ "network.target" ];
      };

      Service = {
        Restart = "always";
        ExecStart = "${pkgs.protonmail-bridge}/bin/protonmail-bridge --no-window --log-level error --noninteractive";
        Environment = "PATH=${pkgs.libsecret}/bin";
      };

      Install = {
        WantedBy = [ "default.target" ];
      };
    };
  };
}
