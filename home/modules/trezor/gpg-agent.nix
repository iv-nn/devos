{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.services.trezor.gpg-agent;
  runAgentScript = pkgs.writeShellScript "run-agent" ''
    export PATH="${makeBinPath [ pkgs.gnupg ]}"
    ${pkgs.trezor_agent}/bin/trezor-gpg-agent -v $*
  '';
in
{
  options = {
    services.trezor.gpg-agent = {
      enable = mkEnableOption "Trezor gpg agent";

      configDir = mkOption {
        type = types.str;
        default = "${config.home.homeDirectory}/.gnupg/trezor";
        description = ''
          The Trezor GnuPG config directory, used to set GNUPGHOME.
          To set it up use `trezor-gpg init "identity"` and remove the run-agent.sh file.
          See https://github.com/romanz/trezor-agent/blob/master/doc/README-GPG.md
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    home = {
      file."${cfg.configDir}/run-agent.sh".source = runAgentScript;

      packages = with pkgs; [
        trezor-suite
        trezor_agent
      ];

      sessionVariables = {
        GNUPGHOME = cfg.configDir;
      };
    };

    systemd.user = {
      services = {
        "trezor-gpg-agent" = {
          Unit = {
            Description = "Trezor gpg agent";
            Requires = "trezor-gpg-agent.socket";
          };
          Service = {
            Type = "simple";
            Environment = [
              "GNUPGHOME=${cfg.configDir}"
              "PATH=${pkgs.gnupg}/bin"
            ];
            ExecStart = "${pkgs.trezor_agent}/bin/trezor-gpg-agent -v";
          };
        };
      };

      sockets = {
        "trezor-gpg-agent" = {
          Unit = {
            Description = "Trezor gpg agent socket";
          };
          Socket = {
            ListenStream = "%t/gnupg/S.gpg-agent";
            FileDescriptorName = "std";
            SocketMode = 600;
            DirectoryMode = 700;
          };
          Install = {
            WantedBy = [ "sockets.target" ];
          };
        };
      };
    };
  };
}
