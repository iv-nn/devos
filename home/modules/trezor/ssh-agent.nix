{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.services.trezor.ssh-agent;
in
{
  options = {
    services.trezor.ssh-agent = {
      enable = mkEnableOption "Trezor ssh agent";
      identity = mkOption {
        type = with types; nullOr str;
        default = null;
        example = "user@example.com";
        description = ''
          The identity used by Trezor ssh agent.
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    assertions = [
      {
        assertion = cfg.identity != null;
        message = "Please specify a Trezor ssh identity";
      }
    ];

    home = {
      packages = with pkgs; [
        trezor-suite
        trezor_agent
      ];

      sessionVariables = {
        SSH_AUTH_SOCK = "$(systemctl show --user --property=Listen trezor-ssh-agent.socket | grep -o '/run.*' | cut -d ' ' -f 1)";
      };
    };

    systemd.user = {
      services = {
        "trezor-ssh-agent" = {
          Unit = {
            Description = "Trezor ssh agent";
            Requires = "trezor-ssh-agent.socket";
          };
          Service = {
            Type = "simple";
            Restart = "always";
            Environment = [ "PATH=${pkgs.openssh}/bin" ];
            ExecStart = "${pkgs.trezor_agent}/bin/trezor-agent --foreground --sock-path %t/trezor-agent/S.ssh --ecdsa-curve-name ed25519 ${cfg.identity}";
          };
        };
      };

      sockets = {
        "trezor-ssh-agent" = {
          Unit = {
            Description = "Trezor ssh agent socket";
          };
          Socket = {
            ListenStream = "%t/trezor-agent/S.ssh";
            FileDescriptorName = "ssh";
            Service = "trezor-ssh-agent.service";
            SocketMode = 600;
            DirectoryMode = 700;
          };
          Install = {
            WantedBy = [ "sockets.target" ];
          };
        };
      };
    };
  };
}
